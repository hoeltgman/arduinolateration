#ifndef MATRIXRANGE_H
#define MATRIXRANGE_H

#include <stddef.h>

namespace LinearAlgebra
{
/**
 * @brief MatrixRange class
 *
 * @tparam N
 */
template <size_t N>
class MatrixRange
{
      public:
        /**
         * @brief Construct a new Matrix Range object
         *
         */
        MatrixRange() : _lbound(1), _ubound(N), _step(1) {}
        /**
         * @brief Construct a new Matrix Range object
         *
         * @param lbound
         */
        MatrixRange(size_t lbound) : _lbound(lbound), _ubound(lbound + N), _step(1) {}
        /**
         * @brief Get the lbound object
         *
         * @return size_t
         */
        size_t
        get_lbound() const
        {
                return _lbound;
        }

      protected:
        size_t _lbound; //!< lower bound
        size_t _ubound; //!< upper bound
        size_t _step;   //!< step size

      private:
};

/**
 * @brief MatrixRowRange
 *
 * @tparam N
 */
template <size_t N>
class MatrixRowRange : public MatrixRange<N>
{
      public:
        /**
         * @brief Construct a new Matrix Row Range object
         *
         * @param lbound
         */
        MatrixRowRange(size_t lbound) : MatrixRange<N>(lbound){};

      protected:
      private:
};

/**
 * @brief MatrixColumnRange
 *
 * @tparam N
 */
template <size_t N>
class MatrixColumnRange : public MatrixRange<N>
{
      public:
        /**
         * @brief Construct a new Matrix Column Range object
         *
         * @param lbound
         */
        MatrixColumnRange(size_t lbound) : MatrixRange<N>(lbound) {}

      protected:
      private:
};

} // namespace LinearAlgebra

#endif // #ifndef MATRIXRANGE_H
