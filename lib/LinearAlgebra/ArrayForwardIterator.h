#ifndef ARRAY_FORWARDITERATOR_H
#define ARRAY_FORWARDITERATOR_H

#include <stddef.h>

namespace LinearAlgebra
{
/**
 * @brief Forward Iterator
 *
 */
template <typename T>
class ForwardIterator
{
      public:
        /**
         * @brief Construct a new Forward Iterator object
         *
         * @param _num
         */
        ForwardIterator(size_t _num = 0) : num(_num), lower_bound(0), upper_bound(0) {}

        /**
         * @brief increment
         *
         * @return ForwardIterator&
         */
        ForwardIterator &
        operator++()
        {
                num = upper_bound >= lower_bound ? num + 1 : num - 1;
                return *this;
        }

        /**
         * @brief increment
         *
         * @return ForwardIterator
         */
        ForwardIterator
        operator++(int)
        {
                ForwardIterator retval = *this;
                ++(*this);
                return retval;
        }

        /**
         * @brief equality check
         *
         * @param other
         * @return true
         * @return false
         */
        bool
        operator==(ForwardIterator other) const
        {
                return num == other.num;
        }

        /**
         * @brief not equal operator
         *
         * @param other
         * @return true
         * @return false
         */
        bool
        operator!=(const ForwardIterator &other) const
        {
                return !(*this == other);
        }

        /**
         * @brief reference operator
         *
         * @return long
         */
        long
        operator*()
        {
                return num;
        }

      protected:
      private:
        size_t lower_bound; //!< lower bound
        size_t upper_bound; //!< upper bound
        size_t num;         //!< position

      public:
        using difference_type = T;         //!< iterator trait
        using value_type      = T;         //!< iterator trait
        using pointer         = const T *; //!< iterator trait
        using reference       = const T &; //!< iterator trait
};
} // namespace LinearAlgebra

#endif // #define ARRAY_FORWARDITERATOR_H
