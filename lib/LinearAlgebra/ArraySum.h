#ifndef ARRAY_SUM_H
#define ARRAY_SUM_H

#include "ArrayExpression.h"

#include <stddef.h>

namespace LinearAlgebra
{
/**
 * @brief Sum of two Arrays
 *
 * @tparam E1
 * @tparam E2
 */
template <typename E1, typename E2>
class ArraySum : public ArrayExpression<ArraySum<E1, E2>>
{
      public:
        /**
         * @brief Construct a new Array Sum object
         *
         * @param u
         * @param v
         */
        ArraySum(E1 const &u, E2 const &v) : _u(u), _v(v) {}

        /**
         * @brief Indexing operator
         *
         * @param i
         * @return float
         */
        float
        operator[](size_t i) const
        {
                return _u[i] + _v[i];
        }

        /**
         * @brief Size operator
         *
         * @return size_t
         */
        size_t
        numel() const
        {
                return _v.numel();
        }

      protected:
      private:
        E1 const &_u; //!< First summand
        E2 const &_v; //!< Second summand

}; // class ArraySum

/**
 * @brief Pointwise sum of two arrays
 *
 * @tparam E1
 * @tparam E2
 * @param u
 * @param v
 * @return ArraySum<E1, E2>
 */
template <typename E1, typename E2>
ArraySum<E1, E2>
operator+(ArrayExpression<E1> const &u, ArrayExpression<E2> const &v)
{
        return ArraySum<E1, E2>(*static_cast<const E1 *>(&u), *static_cast<const E2 *>(&v));
}

} // namespace LinearAlgebra

#endif // #define ARRAY_SUM_H
