#ifndef ARRAY_EXPRESSION_H
#define ARRAY_EXPRESSION_H

#include <math.h>
#include <stddef.h>

namespace LinearAlgebra
{
/**
 * @brief Generic Array Expression class
 *
 * @tparam E
 */
template <typename E>
class ArrayExpression
{
      public:
        /**
         * @brief Indexing operator
         *
         * @param i
         * @return float
         */
        float
        operator[](size_t i) const
        {
                return static_cast<E const &>(*this)[i];
        }
        /**
         * @brief Size operator
         *
         * @return size_t
         */
        size_t
        numel() const
        {
                return static_cast<E const &>(*this).numel();
        }

      protected:
      private:
}; // class ArrayExpression

/**
 * @brief euclidean (dot) product
 *
 * @tparam T
 * @tparam E1
 * @tparam E2
 * @param u
 * @param v
 * @return T
 */
template <typename T, typename E1, typename E2>
T
dot(ArrayExpression<E1> const &u, ArrayExpression<E2> const &v)
{
        T result = 0;
        for (size_t i = 0; i != u.numel(); ++i)
        {
                result += u[i] * v[i];
        }
        return result;
}

/**
 * @brief Euclidean Norm
 *
 * @tparam T
 * @tparam N
 * @param u
 * @return T
 */
template <typename T, typename E1>
T
norm(ArrayExpression<E1> const &u)
{
        T result = sqrt(LinearAlgebra::dot<T>(u, u));
        return result;
}

} // namespace LinearAlgebra

#endif // #define ARRAY_EXPRESSION_H
