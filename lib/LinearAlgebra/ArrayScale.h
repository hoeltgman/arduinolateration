#ifndef ARRAY_SCALE_H
#define ARRAY_SCALE_H

#include "ArrayExpression.h"

#include <stddef.h>

namespace LinearAlgebra
{
/**
 * @brief Scalar times Array expression.
 *
 * @tparam E1
 * @tparam E2
 */
template <typename E1, typename E2>
class ArrayScale : public ArrayExpression<ArrayScale<E1, E2>>
{
      public:
        /**
         * @brief Construct a new Array Scale object
         *
         * @param u
         * @param v
         */
        ArrayScale(E1 const &u, E2 const &v) : _u(u), _v(v) {}

        /**
         * @brief Indexing operator
         *
         * @param i
         * @return float
         */
        float
        operator[](size_t i) const
        {
                return _u * _v[i];
        }

        /**
         * @brief Size operator
         *
         * @return size_t
         */
        size_t
        numel() const
        {
                return _v.numel();
        }

      protected:
      private:
        E1 const &_u; //!< Scalar
        E2 const &_v; //!< Array expression

}; // class ArrayScale

/**
 * @brief Scalar times array
 *
 * @tparam E1
 * @tparam E2
 * @param alpha
 * @param v
 * @return ArrayScale<E1, E2>
 */
template <typename E1, typename E2>
ArrayScale<E1, E2>
operator*(E1 const &alpha, ArrayExpression<E2> const &v)
{
        return ArrayScale<E1, E2>(*static_cast<const E1 *>(&alpha), *static_cast<const E2 *>(&v));
}

/**
 * @brief Array times scalar
 *
 * @tparam E1
 * @tparam E2
 * @param v
 * @param alpha
 * @return ArrayScale<E1, E2>
 */
template <typename E1, typename E2>
ArrayScale<E1, E2>
operator*(ArrayExpression<E2> const &v, E1 const &alpha)
{
        return ArrayScale<E1, E2>(*static_cast<const E1 *>(&alpha), *static_cast<const E2 *>(&v));
}

} // namespace LinearAlgebra

#endif // #define ARRAY_SCALE_H
