#ifndef GIVENSROTATION_H
#define GIVENSROTATION_H

#include "Array.h"

#include <math.h>
#include <stddef.h>
#include <stdint.h>

namespace LinearAlgebra
{
/**
 * @brief Givens rotations
 *
 * @tparam T
 */
template <typename T>
class GivensRotation
{
      public:
        /**
         * @brief Construct a new Givens Rotation object
         *
         * @param row_index
         * @param column_index
         * @param angle
         */
        GivensRotation(size_t row_index, size_t column_index, T angle) :
          _row_index(row_index), _column_index(column_index), _angle(angle)
        {
        }

        /**
         * @brief getter for the row index
         *
         * @return size_t
         */
        size_t
        row_index() const
        {
                return _row_index;
        }

        /**
         * @brief getter for the column index
         *
         * @return size_t
         */
        size_t
        column_index() const
        {
                return _column_index;
        }

        /**
         * @brief getter for the angle
         *
         * @return T
         */
        T
        angle() const
        {
                return _angle;
        }

        /**
         * @brief getter for the entry of the matrix representation
         *
         * @param row_index
         * @param column_index
         * @return T
         */
        T
        at(size_t row_index, size_t column_index) const
        {
                if (row_index == _row_index && column_index == _row_index)
                {
                        return cos(_angle);
                }
                if (row_index == _column_index && column_index == _column_index)
                {
                        return cos(_angle);
                }
                if (row_index == _row_index && column_index == _column_index)
                {
                        return sin(_angle);
                }
                if (row_index == _column_index && column_index == _row_index)
                {
                        return -sin(_angle);
                }
                if (row_index == column_index)
                {
                        return static_cast<T>(1);
                }
                return 0;
        }

        /**
         * @brief Apply Givens rotation onto vector
         *
         * @tparam N
         * @param input
         * @return Array<T, N>
         */
        template <size_t N>
        Array<T, N>
        eval(const Array<T, N> input) const
        {
                Array<T, N> output = input;
                output.set(_row_index, cos(_angle) * input.at(_row_index) + sin(_angle) * input.at(_column_index));
                output.set(_column_index, -sin(_angle) * input.at(_row_index) + cos(_angle) * input.at(_column_index));
                return output;
        }

      private:
        size_t _row_index;    //!< row index of the matrix representation
        size_t _column_index; //!< column index of the matrix representation
        T      _angle;        //!< angle of the rotation
};
} // namespace LinearAlgebra

#endif // #ifndef GIVENSROTATION_H
