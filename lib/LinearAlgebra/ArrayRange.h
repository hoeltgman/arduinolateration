#ifndef ARRAY_RANGE_H
#define ARRAY_RANGE_H

#include "ArrayForwardIterator.h"

#include <stddef.h>

namespace LinearAlgebra
{
/**
 * @brief ArrayRange
 *
 * @tparam FROM
 * @tparam TO
 */
template <typename T, size_t FROM, size_t TO>
class ArrayRange
{
      public:
        /**
         * @brief begin function
         *
         * @return ForwardIterator
         */
        ForwardIterator<T>
        begin()
        {
                return FROM;
        }

        /**
         * @brief end function
         *
         * @return ForwardIterator
         */
        ForwardIterator<T>
        end()
        {
                return TO >= FROM ? TO + 1 : TO - 1;
        }
};
} // namespace LinearAlgebra

#endif // #define ARRAY_RANGE_H
