#ifndef ARRAY_H
#define ARRAY_H

#include "ArrayDiff.h"
#include "ArrayExpression.h"
#include "ArrayRange.h"
#include "ArrayScale.h"
#include "ArraySum.h"
#include "ArrayUMinus.h"

#include <stddef.h>

/**
 * @brief Namespace for Matrices, Vectors, ...
 *
 */
namespace LinearAlgebra
{
/**
 * @brief Array class
 *
 * Array is a fixed size container similar to std::array. It is meant to model
 * vectors in finite dimensional vector spaces. As a such common arithmetic
 * operations are provided. Pointwise operations, such as addition, are
 * implemented as expression templates to maximize their performance. See the
 * documentation of e.g. ArraySum or ArrayExpression.
 *
 * @tparam T type parameter
 * @tparam N size of the container (number of elements of type T)
 */
template <typename T, size_t N>
class Array : public ArrayExpression<Array<T, N>>
{
      public:
        /**
         * @brief Construct a new Array object
         *
         */
        Array(){};

        /**
         * @brief Construct a new Array object from an ArrayExpression
         *
         * @tparam E
         * @param expr
         */
        template <typename E>
        Array(ArrayExpression<E> const &expr)
        {
                for (size_t i = 0; i != expr.numel(); ++i)
                {
                        _data[i] = expr[i];
                }
        }

        /**
         * @brief Indexing operator
         *
         * @param i
         * @return T
         */
        T
        operator[](size_t i) const
        {
                return _data[i];
        }

        /**
         * @brief Indexing operator
         *
         * @param i
         * @return T&
         */
        T &
        operator[](size_t i)
        {
                return _data[i];
        }

        /**
         * @brief Getter for the size of an array
         *
         * @return size_t
         */
        size_t
        numel() const
        {
                return _numel;
        }

        /**
         * @brief indexing function (starts at 1)
         *
         * @param ir
         * @return T
         */
        T
        at(size_t ir) const
        {
                return _data[ir - 1];
        }

        /**
         * @brief indexing function (starts at 1)
         *
         * @param ir
         * @return T&
         */
        T &
        at(size_t ir)
        {
                return _data[ir - 1];
        }

        /**
         * @brief Set the value at a specific position (starts at 1)
         *
         * @param ir
         * @param va
         * @return T
         */
        T
        set(size_t ir, T va)
        {
                _data[ir - 1] = va;
                return _data[ir - 1];
        }

      protected:
      private:
        size_t _numel = N; //!< Size of the container
        T      _data[N];   //!< Data stored in the container

}; // class Array

} // namespace LinearAlgebra

#endif // #define ARRAY_H
