#ifndef HOUSEHOLDERTRANSFORM_H
#define HOUSEHOLDERTRANSFORM_H

#include "Array.h"

#include <stddef.h>
#include <stdint.h>

namespace LinearAlgebra
{
/**
 * @brief Housholder Transforms
 *
 * @tparam T
 * @tparam N
 */
template <typename T, size_t N>
class HouseholderTransform
{
      public:
        /**
         * @brief Construct a new Householder Transform object
         *
         * @param hyperplane_normal_vector
         */
        HouseholderTransform(Array<T, N> hyperplane_normal_vector) : _hyperplane_normal_vector(hyperplane_normal_vector) {}

        /**
         * @brief getter for the hyperplane normal vector
         *
         * @return Array<T, N>
         */
        Array<T, N>
        hyperplane_normal_vector() const
        {
                return _hyperplane_normal_vector;
        }

        /**
         * @brief apply Householder transform onto a vector
         *
         * @param input
         * @return Array<T, N>
         */
        Array<T, N>
        eval(Array<T, N> input) const
        {
                T           normalisation_factor = dot<T>(input, input);
                T           projection_factor    = dot<T>(input, _hyperplane_normal_vector);
                Array<T, N> output               = input - 2 * projection_factor / normalisation_factor * input;
                return output;
        }

      private:
        Array<T, N> _hyperplane_normal_vector;
};
} // namespace LinearAlgebra

#endif // #ifndef HOUSEHOLDERTRANSFORM_H
