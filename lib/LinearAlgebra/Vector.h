#ifndef VECTOR_H
#define VECTOR_H

#include "Array.h"

#include <stddef.h>

namespace LinearAlgebra
{
/**
 * @brief Vector Class
 *
 * @tparam T
 * @tparam N
 */
template <typename T, size_t N>
class Vector : public Array<T, N>
{
      public:
      protected:
      private:
        size_t starting_idx; //!< starting index
};

/**
 * @brief Cross Product
 *
 * @tparam T
 * @tparam E1
 * @tparam E2
 * @param u
 * @param v
 * @return Vector<T, 3>
 */
template <typename T, typename E1, typename E2>
Vector<T, 3>
cross(ArrayExpression<E1> const &u, ArrayExpression<E2> const &v)
{
        Vector<T, 3> result;
        result[0] = u[1] * v[2] - u[2] * v[1];
        result[1] = u[2] * v[0] - u[0] * v[2];
        result[2] = u[0] * v[1] - u[1] * v[0];
        return result;
}

} // namespace LinearAlgebra
#endif // #define VECTOR_H
