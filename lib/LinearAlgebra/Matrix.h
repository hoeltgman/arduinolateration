#ifndef MATRIX_H
#define MATRIX_H

#include "Array.h"
#include "MatrixRange.h"
#include "Vector.h"

#include <stddef.h>

namespace LinearAlgebra
{
/**
 * @brief Matrix class
 *
 * @tparam T
 * @tparam N
 * @tparam M
 */
template <typename T, size_t N, size_t M>
class Matrix : public Array<T, N * M>
{
      public:
        /**
         * @brief Construct a new Matrix object
         *
         */
        Matrix() : _is_symmetric(false){};

        /**
         * @brief Construct a new Matrix object
         *
         * @tparam E
         * @param expr
         */
        template <typename E>
        Matrix(ArrayExpression<E> const &expr) : _is_symmetric(false)
        {
                for (size_t i = 0; i != expr.numel(); ++i)
                {
                        (*this)[i] = expr[i];
                }
        }

        /**
         * @brief Getter for the number of rows
         *
         * @return size_t
         */
        size_t
        nr() const
        {
                return _nr;
        };

        /**
         * @brief Getter for the number of columns
         *
         * @return size_t
         */
        size_t
        nc() const
        {
                return _nc;
        };

        /**
         * @brief indexing function (starts at 1)
         *
         * @param ir row index
         * @param jc column index
         * @return T
         */
        T
        at(size_t ir, size_t jc) const
        {
                return (*this)[(ir - 1) * _nc + jc - 1];
        }

        /**
         * @brief indexing function (starts at 1)
         *
         * @param ir
         * @param jc
         * @return T&
         */
        T &
        at(size_t ir, size_t jc)
        {
                return (*this)[(ir - 1) * _nc + jc - 1];
        }

        /**
         * @brief Set the value at a specific position (starts at 1)
         *
         * @param ir
         * @param jc
         * @param va
         * @return T
         */
        T
        set(size_t ir, size_t jc, T va)
        {
                (*this)[(ir - 1) * _nc + jc - 1] = va;
                return at(ir, jc);
        }

        /**
         * @brief extract a row from the matrix
         *
         * @param ir
         * @return Matrix<T, 1, M>
         */
        Matrix<T, 1, M>
        get_row(size_t ir) const
        {
                Matrix<T, 1, M> row;
                for (size_t ii = 1; ii <= M; ii++)
                {
                        row.set(1, ii, this->at(ir, ii));
                }
                return row;
        }

        /**
         * @brief extract a column from the matrix
         *
         * @param jc
         * @return Matrix<T, N, 1>
         */
        Matrix<T, N, 1>
        get_column(size_t jc) const
        {
                Matrix<T, N, 1> column;
                for (size_t ii = 1; ii <= N; ii++)
                {
                        column.set(ii, 1, this->at(ii, jc));
                }
                return column;
        }

        /**
         * @brief Get the rowrange object
         *
         * @tparam K
         * @param range
         * @return Matrix<T, K, M>
         */
        template <size_t K>
        Matrix<T, K, M>
        get_rowrange(MatrixRowRange<K> const &range) const
        {
                Matrix<T, K, M> columns;
                for (size_t ii = 1; ii <= K; ii++)
                {
                        for (size_t jj = 1; jj <= M; jj++)
                        {
                                columns.set(ii, jj, this->at(range.get_lbound() + ii, jj));
                        }
                }
                return columns;
        }

        /**
         * @brief Get the columnrange object
         *
         * @tparam K
         * @param range
         * @return Matrix<T, N, K>
         */
        template <size_t K>
        Matrix<T, N, K>
        get_columnrange(MatrixColumnRange<K> const &range) const
        {
                Matrix<T, K, M> rows;
                for (size_t ii = 1; ii <= N; ii++)
                {
                        for (size_t jj = 1; jj <= K; jj++)
                        {
                                rows.set(ii, jj, this->at(ii, range.get_lbound() + jj));
                        }
                }
                return rows;
        }

        /**
         * @brief Matrix - Vector product
         *
         * @param b
         * @return Array<T, N>
         */
        Vector<T, N>
        eval(Vector<T, M> const &b) const
        {
                Vector<T, N> result;
                for (size_t jj = 1; jj <= N; jj++)
                {
                        T dot_product = 0;
                        for (size_t ii = 1; ii <= M; ii++)
                        {
                                dot_product += this->at(jj, ii) * b.at(ii);
                        }
                        result.set(jj, dot_product);
                }
                return result;
        }

        /**
         * @brief Matrix - Matrix product
         *
         * @param B
         * @return Matrix<T, N, R>
         */
        template <size_t R>
        Matrix<T, N, R>
        eval(Matrix<T, M, R> const &B) const
        {
                Matrix<T, N, R> result;
                for (size_t ii = 1; ii <= N; ii++)
                {
                        for (size_t jj = 1; jj <= R; jj++)
                        {
                                T dot_product = 0;
                                for (size_t kk = 1; kk <= M; kk++)
                                {
                                        dot_product += this->at(ii, kk) * B.at(kk, jj);
                                }
                                result.set(ii, jj, dot_product);
                        }
                }
                return result;
        }

        /**
         * @brief Matrix transposition
         *
         * @return Matrix<T, M, N>
         */
        Matrix<T, M, N>
        transpose() const
        {
                Matrix<T, M, N> result;
                for (size_t ii = 1; ii <= N; ii++)
                {
                        for (size_t jj = 1; jj <= M; jj++)
                        {
                                result.set(jj, ii, this->at(ii, jj));
                        }
                }
                return result;
        }

      protected:
      private:
        size_t _nr = N;             //!< number of rows
        size_t _nc = M;             //!< number of columns
        size_t _start_idx_rows;     //!< starting index for rows
        size_t _start_idx_cols;     //!< starting index for columns
        bool   _is_symmetric;       //!< flag whether the matrix is symmetric
        bool   _row_major_ordering; //!< how the data is ordered in memory
};

} // namespace LinearAlgebra

#endif // #define MATRIX_H
