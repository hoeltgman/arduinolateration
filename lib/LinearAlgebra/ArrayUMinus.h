#ifndef ARRAY_UMINUS_H
#define ARRAY_UMINUS_H

#include "ArrayExpression.h"

#include <stddef.h>

namespace LinearAlgebra
{
/**
 * @brief Array expression for the unary minus of an Array
 *
 * @tparam E
 */
template <typename E>
class ArrayUMinus : public ArrayExpression<ArrayUMinus<E>>
{
      public:
        /**
         * @brief Construct a new Array U Minus object
         *
         * @param u
         */
        explicit ArrayUMinus(E const &u) : _u(u) {}

        /**
         * @brief Indexing operator
         *
         * @param i
         * @return float
         */
        float
        operator[](size_t i) const
        {
                return -_u[i];
        }

        /**
         * @brief Size of an Array
         *
         * @return size_t
         */
        size_t
        numel() const
        {
                return _u.numel();
        }

      protected:
      private:
        E const &_u; //!< Array Expression

}; // class ArrayUMinus

/**
 * @brief Unary minus of array
 *
 * @tparam E
 * @param u
 * @return ArrayUMinus<E>
 */
template <typename E>
ArrayUMinus<E>
operator-(ArrayExpression<E> const &u)
{
        return ArrayUMinus<E>(*static_cast<const E *>(&u));
}

} // namespace LinearAlgebra

#endif // #define ARRAY_UMINUS_H
