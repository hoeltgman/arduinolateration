#ifndef SPEEDOFSOUND_H
#define SPEEDOFSOUND_H

#include <math.h>

namespace Model
{
/**
 * @brief Compute speed of sound in function of temperature.
 *
 */
class SpeedOfSound
{
      public:
        /**
         * @brief Create instance. Speed of Sound is a singleton.
         *
         * @return SpeedOfSound&
         */
        static SpeedOfSound &
        get_instance()
        {
                static SpeedOfSound instance;
                return instance;
        }

        /**
         * @brief Get the speed
         *
         * @return float
         */
        float
        get_speed() const
        {
                return get_speed(current_temperature_celsius);
        }

        /**
         * @brief Get the speed
         *
         * @param temperature_celsius
         * @return float
         */
        float
        get_speed(float temperature_celsius) const
        {
                float temperature_kelvin = interpret_temperature(temperature_celsius);
                float speed = sqrtf(heat_capacity_ratio * universal_gas_constant * temperature_kelvin / molar_mass); // [m/s]
                return speed;
        }

        /**
         * @brief Set the temperature object
         *
         * @param temperature_celsius
         */
        void
        set_temperature(float temperature_celsius)
        {
                current_temperature_celsius = temperature_celsius;
        }

      private:
        /**
         * @brief Construct a new Speed Of Sound object
         *
         */
        SpeedOfSound() {}

        static float           current_temperature_celsius;       ///< Current Temperature
        static constexpr float universal_gas_constant = 8.3145f;  ///< universal gas constant [J/(mol * K)]
        static constexpr float heat_capacity_ratio    = 1.42f;    ///< heat capacity ratio []
        static constexpr float molar_mass             = 0.02896f; ///< molar mass [kg/mol]
        static constexpr float absolute_zero          = -273.15;  ///< 0 degree kelvin in degree celsius

        /**
         * @brief Convert degree Celsius to degree Kelvin
         *
         * @param temperature_celsius
         * @return float
         */
        static float
        convert_celsius_to_kelvin(float temperature_celsius)
        {
                return temperature_celsius - absolute_zero;
        }

        /**
         * @brief Interpret input temperature and handle special cases.
         * @details Nan and temperatures below absolute zero get set to 0 Kelvin.
         *          Everything else is simply converted to degree Kelvin.
         *
         * @param temperature in degree celsius
         * @return float
         */
        static float
        interpret_temperature(float temperature_celsius)
        {
                float temperature_kelvin = 0.0F;
                if (!(isnan(temperature_celsius) || (temperature_celsius < absolute_zero)))
                {
                        temperature_kelvin = convert_celsius_to_kelvin(temperature_celsius);
                }
                return temperature_kelvin;
        }

      public:
        /**
         * @brief Construct a new Speed Of Sound object
         *
         */
        SpeedOfSound(SpeedOfSound const &) = delete;
        /**
         * @brief = operator
         *
         */
        void
        operator=(SpeedOfSound const &) = delete;
}; // class SpeedOfSound
} // namespace Model

#endif // #ifndef SPEEDOFSOUND_H
