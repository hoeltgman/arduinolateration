#include "UltraSoundSensor.h"

#include "SpeedOfSound.h"
#include "UltraSoundSensorPosition.h"

Model::UltraSoundSensor::UltraSoundSensor() :
  _trig_pin(PIN_UNSET), _echo_pin(PIN_UNSET), _sender_receiver_distance(DISTANCE_UNSET)
{
}

Model::UltraSoundSensor::UltraSoundSensor(const UltraSoundSensorPosition &position,
                                          uint8_t                         trig_pin,
                                          uint8_t                         echo_pin,
                                          float                           sender_receiver_distance) :
  _position(position),
  _trig_pin(trig_pin), _echo_pin(echo_pin), _sender_receiver_distance(sender_receiver_distance)
{
}

uint8_t
Model::UltraSoundSensor::sensor_id() const
{
        uint8_t id = SENSOR_ID_MULTIPLIER * _trig_pin + _echo_pin;
        return id;
}

Model::UltraSoundSensorPosition
Model::UltraSoundSensor::position() const
{
        return _position;
}

void
Model::UltraSoundSensor::reset_sensor() const
{
}

float
Model::UltraSoundSensor::convert_run_time_to_distance(float time, float temperature)
{
        SpeedOfSound &speed_sound = SpeedOfSound::get_instance();
        auto          speed       = speed_sound.get_speed(temperature);
        float         distance    = time / 2.0 * speed * 1.0e-3;
        return distance;
}
