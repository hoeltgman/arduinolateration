#ifndef ULTRASOUNDSENSOR_H
#define ULTRASOUNDSENSOR_H

#include "UltraSoundSensorPosition.h"

#include <stddef.h>
#include <stdint.h>

namespace Model
{
/**
 * @brief Ultrasound sensor
 *
 */
class UltraSoundSensor
{
      public:
        /**
         * @brief Construct a new Ultra Sound Sensor object
         *
         */
        UltraSoundSensor();

        /**
         * @brief Construct a new Ultra Sound Sensor object
         *
         * @param position
         * @param trig_pin
         * @param echo_pin
         * @param sender_receiver_distance
         */
        UltraSoundSensor(const UltraSoundSensorPosition &position,
                         uint8_t                         trig_pin,
                         uint8_t                         echo_pin,
                         float                           sender_receiver_distance);
        /**
         * @brief Get sensor id
         *
         * @return uint8_t
         */
        uint8_t
        sensor_id() const;

        /**
         * @brief Return position of sensor
         *
         * @return float(&)[3]
         */
        UltraSoundSensorPosition
        position() const;

        /**
         * @brief set trig pin to LOW
         *
         */
        void
        reset_sensor() const;

      protected:
      private:
        /**
         * @brief Convert run time to distance
         *
         * @param time
         * @param temperature
         * @return float
         */
        static float
        convert_run_time_to_distance(float time, float temperature);

        static constexpr uint8_t SENSOR_ID_MULTIPLIER = 15;        //!< multiplier for converting pin tuple into id
        static constexpr uint8_t PIN_UNSET            = UINT8_MAX; //!< default value for unset pins
        static constexpr float   DISTANCE_UNSET       = -1.0;      //!< default value for unset sender - receiver distances
        UltraSoundSensorPosition _position;                        //!< Sensor coordinates (x,y,z)
        uint8_t                  _trig_pin;                        //!< trig pin index
        uint8_t                  _echo_pin;                        //!< echo pin index
        float                    _sender_receiver_distance;        //!< distance between receiver and sender
};
} // namespace Model

#endif // #ifndef ULTRASOUNDSENSOR_H
