#ifndef ULTRASOUNDSENSORPOSITION_H
#define ULTRASOUNDSENSORPOSITION_H

#include <stddef.h>
#include <stdint.h>

namespace Model
{
/**
 * @brief Ultrasound Sensor position
 *
 */
class UltraSoundSensorPosition
{
      public:
        /**
         * @brief Construct a new Ultra Sound Sensor Position object
         *
         */
        UltraSoundSensorPosition() : _x(0), _y(0), _z(0){};

        /**
         * @brief Construct a new Ultra Sound Sensor Position object
         *
         * @param x
         * @param y
         * @param z
         */
        UltraSoundSensorPosition(float x, float y, float z) : _x(x), _y(y), _z(z){};

        /**
         * @brief Indexing operator
         *
         * @param idx
         * @return float&
         */
        float
        operator[](size_t idx) const;

        /**
         * @brief Getter x coordinate
         *
         * @return float
         */
        float
        x() const;

        /**
         * @brief Getter y coordinate
         *
         * @return float
         */
        float
        y() const;

        /**
         * @brief Getter z coordinate
         *
         * @return float
         */
        float
        z() const;

        auto
        serialize(unsigned char *buffer) const -> unsigned char *;

        auto
        deserialize(unsigned char *buffer) -> unsigned char *;

        static constexpr size_t serial_buffer_size = 3 * sizeof(float);

      protected:
      private:
        float _x; //!< x coordinate
        float _y; //!< y coordinate
        float _z; //!< z coordinate

}; // class UltraSoundSensorPosition
} // namespace Model

#endif // #ifndef ULTRASOUNDSENSORSYSTEM_H
