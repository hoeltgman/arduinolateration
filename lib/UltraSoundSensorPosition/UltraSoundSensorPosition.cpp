#include "UltraSoundSensorPosition.h"

#include "Serialize.h"

#include <stddef.h>
#include <stdint.h>

namespace Model
{
auto
UltraSoundSensorPosition::operator[](size_t idx) const -> float
{
        switch (idx % 3)
        {
                case 0:
                        return _x;
                case 1:
                        return _y;
                case 2:
                        return _z;
                default:
                        return _x;
        }
}

auto
UltraSoundSensorPosition::x() const -> float
{
        return _x;
}

auto
UltraSoundSensorPosition::y() const -> float
{
        return _y;
}

auto
UltraSoundSensorPosition::z() const -> float
{
        return _z;
}

auto
UltraSoundSensorPosition::serialize(unsigned char *buffer) const -> unsigned char *
{
        buffer = Utility::serialize(_x, buffer);
        buffer = Utility::serialize(_y, buffer);
        buffer = Utility::serialize(_z, buffer);
        return buffer;
}

auto
UltraSoundSensorPosition::deserialize(unsigned char *buffer) -> unsigned char *
{
        buffer = Utility::deserialize(&_x, buffer);
        buffer = Utility::deserialize(&_y, buffer);
        buffer = Utility::deserialize(&_z, buffer);
        return buffer;
}

} // namespace Model
