#include "UltraSoundSensorSystem.h"

void
Model::UltraSoundSensorSystem::append(const Model::UltraSoundSensor &sensor)
{
        if (_count_sensors < MAX_NUM_SENSORS - 1)
        {
                _count_sensors++;
                _system[_count_sensors] = sensor;
        }
}

Model::EchoMeasurement
Model::UltraSoundSensorSystem::record_measurement(float temperature)
{
        Model::EchoMeasurement measurement;
        for (uint8_t ii = 0; ii < _count_sensors; ii++)
        {
                // measurement.append(_system[ii].record_sample(temperature));
        }
        return measurement;
}

Model::UltraSoundSensor
Model::UltraSoundSensorSystem::get_sensor(uint8_t idx)
{
        return _system[idx];
}
