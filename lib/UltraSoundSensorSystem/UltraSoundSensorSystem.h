#ifndef ULTRASOUNDSENSORSYSTEM_H
#define ULTRASOUNDSENSORSYSTEM_H

#include "EchoMeasurement.h"
#include "UltraSoundSensor.h"

#include <stddef.h>
#include <stdint.h>

namespace Model
{
/**
 * @brief Ultrasound Sensor System
 *
 */
class UltraSoundSensorSystem
{
      public:
        /**
         * @brief Construct a new Ultra Sound Sensor System object
         *
         */
        UltraSoundSensorSystem() : _count_sensors(0){};

        /**
         * @brief Append a Sensor to the system
         *
         * @param sensor
         */
        void
        append(const UltraSoundSensor &sensor);

        /**
         * @brief Get a specific sensor from the system
         */
        UltraSoundSensor
        get_sensor(uint8_t idx);

        /**
         * @brief record echo measurment
         *
         * @param temperature
         */
        EchoMeasurement
        record_measurement(float temperature);

      protected:
      private:
        static constexpr uint8_t MAX_NUM_SENSORS = 6;      ///< Maximal number of sensors
        uint8_t                  _count_sensors;           ///< Current number of sensors
        UltraSoundSensor         _system[MAX_NUM_SENSORS]; ///< Sensor system

}; // class UltraSoundSensorSystem
} // namespace Model

#endif // #ifndef ULTRASOUNDSENSORSYSTEM_H
