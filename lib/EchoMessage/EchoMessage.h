#ifndef ECHOMESSAGE_H
#define ECHOMESSAGE_H

#include "EchoMeasurement.h"

namespace Model
{
/**
 * @brief Echo Message Class
 *
 */
class EchoMessage
{
      public:
        /**
         * @brief Construct a new Echo Message object
         *
         */
        EchoMessage() : _count_measurements(0){};

        /**
         * @brief Append a measurement
         *
         * @param measurement
         */
        void
        append(const EchoMeasurement &measurement);

        /**
         * @brief Convert Message to a char buffer that can be send over Serial interface.
         */
        auto
        serialize(unsigned char *buffer) const -> unsigned char *;

        /**
         * @brief Reconstruct message from char buffer.
         */
        auto
        deserialize(unsigned char *buffer) -> unsigned char *;

        static constexpr uint8_t MAX_NUM_MEASUREMENTS = 3; ///< Maximal number of measurements in a message
        static constexpr size_t  serial_buffer_size =
            sizeof(uint8_t) + MAX_NUM_MEASUREMENTS * EchoMeasurement::serial_buffer_size;

      protected:
      private:
        uint8_t         _count_measurements;                 ///< Current number of measurements in a message
        EchoMeasurement _measurements[MAX_NUM_MEASUREMENTS]; ///< Echo measurements
};
} // namespace Model

#endif // #define ECHOMESSAGE_H
