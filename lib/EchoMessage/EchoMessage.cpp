#include "EchoMessage.h"

#include "EchoMeasurement.h"
#include "Serialize.h"

void
Model::EchoMessage::append(const Model::EchoMeasurement &measurement)
{
        if (_count_measurements < MAX_NUM_MEASUREMENTS - 1)
        {
                _count_measurements++;
                _measurements[_count_measurements] = measurement;
        }
}

auto
Model::EchoMessage::serialize(unsigned char *buffer) const -> unsigned char *
{
        buffer = Utility::serialize(_count_measurements, buffer);
        for (const auto &measurement : _measurements)
        {
                buffer = measurement.serialize(buffer);
        }
        return buffer;
}

auto
Model::EchoMessage::deserialize(unsigned char *buffer) -> unsigned char *
{
        buffer = Utility::deserialize(&_count_measurements, buffer);
        for (auto &measurement : _measurements)
        {
                buffer = measurement.deserialize(buffer);
        }
        return buffer;
}
