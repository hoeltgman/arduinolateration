#ifndef ECHOSIGNAL_H
#define ECHOSIGNAL_H

#include <stddef.h>

namespace Model
{
/**
 * @brief Echo signal class
 *
 */
class EchoSignal
{
      public:
        /**
         * @brief Construct a new Echo Signal object
         *
         */
        EchoSignal()
        {
                current_cycle_idx = 0;
                measurement_idx   = 0;
        }

        /**
         * @brief update cycle history
         *
         */
        void
        update_cycle_history();

        /**
         * @brief Set the echo distance object
         *
         * @param echo_distance
         * @return float
         */
        float
        set_echo_distance(float echo_distance);

        /**
         * @brief Get the last echo distance object
         *
         * @return float
         */
        float
        get_last_echo_distance();

      protected:
      private:
        static constexpr size_t max_num_cycles  = 10; //!< maximal number of cycles
        static constexpr size_t max_num_sensors = 6;  //!< maximal number of supported sensors
        static const size_t     num_cycles;           //!< current number of cylces that are stored
        static const size_t     num_sensors;          //!< current number of sensors in use

        size_t current_cycle_idx; //!< between 0 and max_num_cycles - 1
        size_t measurement_idx;   //!< between 0 and max_num_sensors - 1

        /**
         * @brief The echo distances of each sensor from the all stored cycles.
         *
         * echo_distance_sensor[0] is always the most recent measurement.
         */
        float echo_distance_sensor[max_num_cycles][max_num_sensors] = {{0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}};
};
} // namespace Model

#endif // #ifndef ECHOSIGNAL_H
