#include "EchoSignal.h"

void
Model::EchoSignal::update_cycle_history()
{
        if (current_cycle_idx < max_num_cycles - 1)
        {
                current_cycle_idx++;
        }
        else
        {
                current_cycle_idx = 0;
        }
        measurement_idx = 0;
        return;
}

float
Model::EchoSignal::set_echo_distance(float echo_distance)
{
        echo_distance_sensor[current_cycle_idx][measurement_idx] = echo_distance;
        if (measurement_idx < max_num_sensors - 1)
        {
                measurement_idx++;
        }
        else
        {
                measurement_idx = 0;
        }
        return echo_distance;
}

float
Model::EchoSignal::get_last_echo_distance()
{
        float last_distance = -1.0f;
        if (measurement_idx == 0)
        {
                last_distance = echo_distance_sensor[current_cycle_idx][max_num_sensors - 1];
        }
        else
        {
                last_distance = echo_distance_sensor[current_cycle_idx][measurement_idx - 1];
        }

        return last_distance;
}
