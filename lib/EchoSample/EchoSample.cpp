#include "EchoSample.h"

#include "Serialize.h"

namespace Model
{
auto
EchoSample::distance() const -> float
{
        return _echodistance;
}

auto
EchoSample::serialize(unsigned char *buffer) const -> unsigned char *
{
        buffer = _sender.serialize(buffer);
        buffer = _receiver.serialize(buffer);
        buffer = Utility::serialize(_echodistance, buffer);
        return buffer;
}

auto
EchoSample::deserialize(unsigned char *buffer) -> unsigned char *
{
        buffer = _sender.deserialize(buffer);
        buffer = _receiver.deserialize(buffer);
        buffer = Utility::deserialize(&_echodistance, buffer);
        return buffer;
}

auto
EchoSample::sender() const -> UltraSoundSensorPosition
{
        return _sender;
}

auto
EchoSample::receiver() const -> UltraSoundSensorPosition
{
        return _receiver;
}
} // namespace Model
