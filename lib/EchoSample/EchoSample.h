#ifndef ECHOSAMPLE_H
#define ECHOSAMPLE_H

#include "UltraSoundSensorPosition.h"

namespace Model
{
/**
 * @brief Echo Sample class
 *
 */
class EchoSample
{
      public:
        /**
         * @brief Construct a new Echo Sample object
         *
         */
        EchoSample() : _echodistance(_DISTANCE_UNSET){};
        /**
         * @brief Construct a new Echo Sample object
         *
         * @param sender
         * @param receiver
         * @param echodistance
         */
        EchoSample(UltraSoundSensorPosition sender, UltraSoundSensorPosition receiver, float echodistance) :
          _sender(sender), _receiver(receiver), _echodistance(echodistance)
        {
        }

        /**
         * @brief Convert Message to a char buffer that can be send over Serial interface.
         */
        auto
        serialize(unsigned char *buffer) const -> unsigned char *;

        /**
         * @brief Reconstruct message from char buffer.
         */
        auto
        deserialize(unsigned char *buffer) -> unsigned char *;

        /**
         * @brief getter echo distance
         *
         * @return float
         */
        float
        distance() const;

        auto
        sender() const -> UltraSoundSensorPosition;

        auto
        receiver() const -> UltraSoundSensorPosition;

        static constexpr size_t serial_buffer_size = 2 * UltraSoundSensorPosition::serial_buffer_size + sizeof(float);

      protected:
      private:
        static constexpr float   _DISTANCE_UNSET = -1.0; ///< default distance
        UltraSoundSensorPosition _sender;                ///< Sending sensor
        UltraSoundSensorPosition _receiver;              ///< Receiving sensor
        float                    _echodistance;          ///< Measured echo distance
};
} // namespace Model

#endif // #ifndef ECHOSAMPLE_H
