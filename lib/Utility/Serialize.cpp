#include "Serialize.h"

#include <stddef.h>
#include <stdint.h>

namespace Utility
{
auto
serialize(float value, unsigned char *buffer) -> unsigned char *
{
        unsigned int ivalue = *((unsigned int *) &value); // assumes 32-bit "unsigned int"
        buffer[0]           = ivalue >> 24;
        buffer[1]           = ivalue >> 16;
        buffer[2]           = ivalue >> 8;
        buffer[3]           = ivalue;
        return buffer + 4;
}

auto
deserialize(float *value, unsigned char *buffer) -> unsigned char *
{
        unsigned int ivalue = (buffer[0] << 24) + (buffer[1] << 16) + (buffer[2] << 8) + buffer[3];
        value[0]            = *((float *) &ivalue);
        return buffer + 4;
}

auto
serialize(uint8_t value, unsigned char *buffer) -> unsigned char *
{
        buffer[0] = value;
        return buffer + 1;
}

auto
deserialize(uint8_t *value, unsigned char *buffer) -> unsigned char *
{
        value[0] = buffer[0];
        return buffer + 1;
}

} // namespace Utility
