add_library( Utility
             Serialize.cpp )

target_link_libraries ( Utility
    # Enable sanitizers for gcc
    PUBLIC "$<$<AND:$<CONFIG:Debug>,$<BOOL:${ENABLE_SANITIZERS}>,$<CXX_COMPILER_ID:GNU>>:-fsanitize=address;-fsanitize=pointer-compare;-fsanitize=leak;-fsanitize=undefined;-fno-omit-frame-pointer>"
    # Enable code coverage for gcc
    PUBLIC "$<$<AND:$<CONFIG:Debug>,$<BOOL:${ENABLE_COVERAGE}>,$<CXX_COMPILER_ID:GNU>>:--coverage>"
    # Enable sanitizers for clang
    PUBLIC "$<$<AND:$<CONFIG:Debug>,$<BOOL:${ENABLE_SANITIZERS}>,$<CXX_COMPILER_ID:Clang,AppleClang>>:-fsanitize=address;-fsanitize=leak;-fsanitize=leak;-fsanitize=undefined;-fno-omit-frame-pointer>"
    # Enable memory sanitizer for clang
    PUBLIC "$<$<AND:$<CONFIG:Debug>,$<BOOL:${ENABLE_MEMORY_SANITIZER}>,$<CXX_COMPILER_ID:Clang,AppleClang>>:-fsanitize=memory;-fno-omit-frame-pointer>"
    # Enable code coverage for clang
    PUBLIC "$<$<AND:$<CONFIG:Debug>,$<BOOL:${ENABLE_COVERAGE}>,$<CXX_COMPILER_ID:Clang,AppleClang>>:-fprofile-instr-generate>"
)

target_compile_options ( Utility
    # Debugging Flags Visual Studio
    PUBLIC "$<$<AND:$<CONFIG:Debug>,$<CXX_COMPILER_ID:MSVC>>:/Wall;/WX>"
    # Release Flags Visual Studio
    PUBLIC "$<$<AND:$<CONFIG:Release>,$<CXX_COMPILER_ID:MSVC>>:/W3>"
    # Debugging Flags gcc and clang (common flags)
    PUBLIC "$<$<AND:$<CONFIG:Debug>,$<CXX_COMPILER_ID:GNU,Clang,AppleClang>>:-Wall;-Wextra;-pedantic;-pedantic-errors>"
    # Release Flags gcc and clang (common flags)
    PUBLIC "$<$<AND:$<CONFIG:Release>,$<CXX_COMPILER_ID:GNU,Clang,AppleClang>>:-Wall>"
)