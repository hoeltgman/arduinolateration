#ifndef SERIALIZE_H
#define SERIALIZE_H

#include <stddef.h>
#include <stdint.h>

namespace Utility
{
auto
serialize(float value, unsigned char *buffer) -> unsigned char *;

auto
deserialize(float *value, unsigned char *buffer) -> unsigned char *;

auto
serialize(uint8_t value, unsigned char *buffer) -> unsigned char *;

auto
deserialize(uint8_t *value, unsigned char *buffer) -> unsigned char *;

} // namespace Utility

#endif // #ifndef SERIALIZE_H