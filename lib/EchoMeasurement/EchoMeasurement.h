#ifndef ECHOMEASUREMENT_H
#define ECHOMEASUREMENT_H

#include "EchoSample.h"

#include <stddef.h>
#include <stdint.h>

namespace Model
{
/**
 * @brief Echo Measurement class.
 *
 */
class EchoMeasurement
{
      public:
        /**
         * @brief Construct a new Echo Measurement object
         *
         */
        EchoMeasurement() : _count_samples(0){};

        /**
         * @brief Append a sample to the measurement
         *
         * @param sample
         */
        void
        append(const EchoSample &sample);

        /**
         * @brief Convert Message to a char buffer that can be send over Serial interface.
         */
        auto
        serialize(unsigned char *buffer) const -> unsigned char *;

        /**
         * @brief Reconstruct message from char buffer.
         */
        auto
        deserialize(unsigned char *buffer) -> unsigned char *;

        static constexpr uint8_t MAX_NUM_SAMPLES    = 3; ///< Maximal number of sensors
        static constexpr size_t  serial_buffer_size = sizeof(uint8_t) + MAX_NUM_SAMPLES * EchoSample::serial_buffer_size;

      protected:
      private:
        uint8_t    _count_samples;            ///< Current number of sensors
        EchoSample _samples[MAX_NUM_SAMPLES]; ///< Samples in the measurement
};
} // namespace Model

#endif // #ifndef ECHOMEASUREMENT_H
