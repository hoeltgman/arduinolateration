#include "EchoMeasurement.h"

#include "EchoSample.h"
#include "Serialize.h"

void
Model::EchoMeasurement::append(const Model::EchoSample &sample)
{
        if (_count_samples < MAX_NUM_SAMPLES - 1)
        {
                _count_samples++;
                _samples[_count_samples] = sample;
        }
}

auto
Model::EchoMeasurement::serialize(unsigned char *buffer) const -> unsigned char *
{
        buffer = Utility::serialize(_count_samples, buffer);
        for (auto &sample : _samples)
        {
                buffer = sample.serialize(buffer);
        }
        return buffer;
}

auto
Model::EchoMeasurement::deserialize(unsigned char *buffer) -> unsigned char *
{
        buffer = Utility::deserialize(&_count_samples, buffer);
        for (auto &sample : _samples)
        {
                buffer = sample.deserialize(buffer);
        }
        return buffer;
}
