// SPDX-License-Identifier: MIT

#ifndef CONFIG_H
#define CONFIG_H

#include <stddef.h> // for size_t
#include <stdint.h> // for uint8_t

/**
 * @brief configuration containing important settings on hardware setup.
 *
 */
namespace Config
{
constexpr unsigned long DEFAULT_BAUD_RATE = 9600;
#ifndef UPPER_ROW_VARIANT
constexpr uint8_t POSITION_IDX_OFFSET = 0; // Upper row sensor positions
#else
constexpr uint8_t POSITION_IDX_OFFSET = 3; // Lower row sensor positions
#endif
/**
 * @brief Controller component configuration
 *
 */
namespace Controller
{
constexpr size_t MAX_NUM_MEASUREMENTS_PER_ROW = 3; //!< Maximal number of measurements in a message
} // namespace Controller

/**
 * @brief Viewer component Configuration
 *
 */
namespace Viewer
{
} // namespace Viewer

/**
 * @brief Model component configuration
 *
 */
namespace Model
{
// Ultrasound Sensor Config
constexpr size_t MAX_NUM_SENSORS       = 6;    //!< Maximal number of ultrasound sensors
constexpr size_t NUM_SENSORS_PER_ROW   = 3;    //!< Number of sensor in one row
constexpr float  INTRA_SENSOR_DISTANCE = 25.0; //!< Distance between transmitter and receiver [mm] of the same sensor

/**
 * @brief Absolute coordinates of Sensor pair centre in IR^3 [mm]
 *
 * These coordinates shall be neither colinear, nor coplanar to ensure
 * that the solution of the lateration is unique.
 * These coordinates refer to the sensor centre, i.e. the half of
 * INTRA_SENSOR_DISTANCE to get the exact coordinates
 */
constexpr float SENSOR_POS[MAX_NUM_SENSORS][3] =
    {{0.0F, 0.0F, 0.0F}, {1.0F, 0.0F, 0.0F}, {0.0F, 1.0F, 0.0F}, {0.0F, 0.0F, 1.0F}, {0.0F, 1.0F, 1.0F}, {1.0F, 1.0F, 1.0F}};

/// TRIG Pins are connected to white cables
/// ECHO Pins are connected to yellow cables
constexpr uint8_t TRIG_PIN_SENSOR_IDX[NUM_SENSORS_PER_ROW] = {9, 7, 5}; //!< TRIG pin sensor indices for SR04 Sensors.
constexpr uint8_t ECHO_PIN_SENSOR_IDX[NUM_SENSORS_PER_ROW] = {8, 6, 4}; //!< ECHO pin sensor indices for SR04 Sensors.

constexpr uint8_t RX_PORT_UNO        = 10; //!< RX software serial port UNO -> MEGA
constexpr uint8_t TX_PORT_UNO        = 11; //!< TX software serial port UNO -> MEGA
constexpr uint8_t RX_PORT_MEGA_LEFT  = 10; //!< RX software serial port MEGA -> UNO (left side)
constexpr uint8_t TX_PORT_MEGA_LEFT  = 11; //!< TX software serial port MEGA -> UNO (left side)
constexpr uint8_t RX_PORT_MEGA_RIGHT = 10; //!< RX software serial port MEGA -> UNO (right side)
constexpr uint8_t TX_PORT_MEGA_RIGHT = 11; //!< TX software serial port MEGA -> UNO (right side)

} // namespace Model
} // namespace Config

#endif // #ifndef CONFIG_H
