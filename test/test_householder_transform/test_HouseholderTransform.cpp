// SPDX-License-Identifier: MIT

#include "test_HouseholderTransform.hpp"

#include "Array.h"
#include "HouseholderTransform.h"

#include <unity.h>

#ifdef NON_PLATFORMIO_COMPILATION

/*
 * These functions are intended to be called before and after each test.
 * If using unity directly, these will need to be provided for each test
 * executable built. If you are using the test runner generator and/or
 * Ceedling, these are optional.
 */

/**
 * @brief set up function
 *
 */
void
setUp(){};

/**
 * @brief tear down function
 *
 */
void
tearDown(){};

/*
 * These functions are intended to be called at the beginning and end of an
 * entire test suite.  suiteTearDown() is passed the number of tests that
 * failed, and its return value becomes the exit code of main(). If using
 * Unity directly, you're in charge of calling these if they are desired.
 * If using Ceedling or the test runner generator, these will be called
 * automatically if they exist.
 */

/**
 * @brief suite set up function
 *
 */
void
suiteSetUp(){};

/**
 * @brief suite tear down function
 *
 */
int
suiteTearDown(int num_failures){};

#endif // #ifdef NON_PLATFORMIO_COMPILATION

void
test_normal_vector_getter()
{
        LinearAlgebra::Array<float, 3> expected_normal_vector;
        expected_normal_vector[0] = 1;
        expected_normal_vector[1] = 0;
        expected_normal_vector[2] = 0;
        LinearAlgebra::HouseholderTransform<float, 3> ht(expected_normal_vector);
        LinearAlgebra::Array<float, 3>                computed_normal_vector = ht.hyperplane_normal_vector();
        TEST_ASSERT_EQUAL_FLOAT(expected_normal_vector[0], computed_normal_vector[0]);
        TEST_ASSERT_EQUAL_FLOAT(expected_normal_vector[1], computed_normal_vector[1]);
        TEST_ASSERT_EQUAL_FLOAT(expected_normal_vector[2], computed_normal_vector[2]);
}

void
test_eval()
{
        LinearAlgebra::Array<float, 2> normal_vector;
        normal_vector[0] = 1;
        normal_vector[1] = 0;
        LinearAlgebra::HouseholderTransform<float, 2> ht(normal_vector);

        LinearAlgebra::Array<float, 2> input_vector;
        input_vector[0] = 0;
        input_vector[1] = 1;

        LinearAlgebra::Array<float, 2> output_vector = ht.eval(input_vector);
        TEST_ASSERT_EQUAL_FLOAT(0, output_vector[0]);
        TEST_ASSERT_EQUAL_FLOAT(1, output_vector[1]);
}

void
process()
{
        UNITY_BEGIN();
        RUN_TEST(test_normal_vector_getter);
        RUN_TEST(test_eval);
        UNITY_END();
}

#ifdef ARDUINO

#include <Arduino.h>

void
setup()
{
        delay(5000);
        process();
}

void
loop()
{
        delay(100);
}

#else

auto
main() -> int
{
        UNITY_BEGIN();
        process();
        return UNITY_END();
}

#endif // #ifdef ARDUINO
