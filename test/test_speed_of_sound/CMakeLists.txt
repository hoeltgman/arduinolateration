CREATE_UNIT_TEST (
  CPP_NAME              test_SpeedOfSound.cpp
  BINARY_NAME           test_SpeedOfSound_app
  COVERAGE_TARGET_NAME  test_SpeedOfSound_app_coverage
  DEPENDENCIES          SpeedOfSound Unity
  INCLUDES              ${CMAKE_SOURCE_DIR}/lib/SpeedOfSound
)
