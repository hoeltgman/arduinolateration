// SPDX-License-Identifier: MIT

#include "test_SpeedOfSound.hpp"

#include "SpeedOfSound.h"

#include <cmath>
#include <unity.h>

#ifdef NON_PLATFORMIO_COMPILATION

/*
 * These functions are intended to be called before and after each test.
 * If using unity directly, these will need to be provided for each test
 * executable built. If you are using the test runner generator and/or
 * Ceedling, these are optional.
 */

/**
 * @brief set up function
 *
 */
void
setUp(){};

/**
 * @brief tear down function
 *
 */
void
tearDown(){};

/*
 * These functions are intended to be called at the beginning and end of an
 * entire test suite.  suiteTearDown() is passed the number of tests that
 * failed, and its return value becomes the exit code of main(). If using
 * Unity directly, you're in charge of calling these if they are desired.
 * If using Ceedling or the test runner generator, these will be called
 * automatically if they exist.
 */

/**
 * @brief suite set up function
 *
 */
void
suiteSetUp(){};

/**
 * @brief suite tear down function
 *
 */
int
suiteTearDown(int num_failures){};

#endif // #ifdef NON_PLATFORMIO_COMPILATION

static Model::SpeedOfSound &speed_sound = Model::SpeedOfSound::get_instance();

void
test_compute_speed_of_sound_0_kelvin()
{
        const float temperature             = -273.15F; // 0 degrees Kelvin
        const float expected_speed_of_sound = 0.0F;
        const float computed_speed_of_sound = speed_sound.get_speed(temperature);

        TEST_ASSERT_EQUAL_FLOAT(expected_speed_of_sound, computed_speed_of_sound);
}

void
test_compute_speed_of_sound_0_celsius()
{
        const float temperature             = 0.0F;
        const float expected_speed_of_sound = 333.7056F;
        const float computed_speed_of_sound = speed_sound.get_speed(temperature);

        TEST_ASSERT_EQUAL_FLOAT(expected_speed_of_sound, computed_speed_of_sound);
}

void
test_compute_speed_of_sound_100_celsius()
{
        const float temperature             = 100.0F;
        const float expected_speed_of_sound = 390.0359F;
        const float computed_speed_of_sound = speed_sound.get_speed(temperature);

        TEST_ASSERT_EQUAL_FLOAT(expected_speed_of_sound, computed_speed_of_sound);
}

void
test_compute_speed_of_sound_nan()
{
        // Nan gets interpreted as 0 Kelvin
        const float temperature             = std::nan("");
        const float expected_speed_of_sound = 0.0F;
        const float computed_speed_of_sound = speed_sound.get_speed(temperature);

        TEST_ASSERT_EQUAL_FLOAT(expected_speed_of_sound, computed_speed_of_sound);
}

void
test_compute_speed_of_sound_impossible_temperature()
{
        // Below 0 Kelvin gets interpreted as 0 Kelvin
        const float temperature             = -300.0F;
        const float expected_speed_of_sound = 0.0F;
        const float computed_speed_of_sound = speed_sound.get_speed(temperature);

        TEST_ASSERT_EQUAL_FLOAT(expected_speed_of_sound, computed_speed_of_sound);
}

void
test_compute_speed_of_sound_no_argument_valid_value()
{
        const float temperature             = 0;
        const float expected_speed_of_sound = 333.7056F;
        speed_sound.set_temperature(temperature);
        const float computed_speed_of_sound = speed_sound.get_speed();

        TEST_ASSERT_EQUAL_FLOAT(expected_speed_of_sound, computed_speed_of_sound);
}

void
test_compute_speed_of_sound_no_argument_invalid_value()
{
        // Below 0 Kelvin gets interpreted as 0 Kelvin
        const float temperature             = -300.0F;
        const float expected_speed_of_sound = 0.0F;
        speed_sound.set_temperature(temperature);
        const float computed_speed_of_sound = speed_sound.get_speed();

        TEST_ASSERT_EQUAL_FLOAT(expected_speed_of_sound, computed_speed_of_sound);
}

void
test_compute_speed_of_sound_no_argument_nan()
{
        // Below 0 Kelvin gets interpreted as 0 Kelvin
        const float temperature             = std::nan("");
        const float expected_speed_of_sound = 0.0F;
        speed_sound.set_temperature(temperature);
        const float computed_speed_of_sound = speed_sound.get_speed();

        TEST_ASSERT_EQUAL_FLOAT(expected_speed_of_sound, computed_speed_of_sound);
}

void
process()
{
        UNITY_BEGIN();
        RUN_TEST(test_compute_speed_of_sound_0_kelvin);
        RUN_TEST(test_compute_speed_of_sound_0_celsius);
        RUN_TEST(test_compute_speed_of_sound_100_celsius);
        RUN_TEST(test_compute_speed_of_sound_nan);
        RUN_TEST(test_compute_speed_of_sound_impossible_temperature);
        RUN_TEST(test_compute_speed_of_sound_no_argument_valid_value);
        RUN_TEST(test_compute_speed_of_sound_no_argument_invalid_value);
        RUN_TEST(test_compute_speed_of_sound_no_argument_nan);
        UNITY_END();
}

#ifdef ARDUINO

#include <Arduino.h>
void
setup()
{
        delay(5000);
        process();
}

void
loop()
{
        digitalWrite(13, HIGH);
        delay(100);
        digitalWrite(13, LOW);
        delay(500);
}

#else

auto
main() -> int
{
        UNITY_BEGIN();
        process();
        return UNITY_END();
}

#endif // #ifdef ARDUINO
