// SPDX-License-Identifier: MIT

#include "test_EchoSample.hpp"

#include "EchoSample.h"
#include "UltraSoundSensorPosition.h"

#include <unity.h>

#ifdef NON_PLATFORMIO_COMPILATION

/*
 * These functions are intended to be called before and after each test.
 * If using unity directly, these will need to be provided for each test
 * executable built. If you are using the test runner generator and/or
 * Ceedling, these are optional.
 */

/**
 * @brief set up function
 *
 */
void
setUp(){};

/**
 * @brief tear down function
 *
 */
void
tearDown(){};

/*
 * These functions are intended to be called at the beginning and end of an
 * entire test suite.  suiteTearDown() is passed the number of tests that
 * failed, and its return value becomes the exit code of main(). If using
 * Unity directly, you're in charge of calling these if they are desired.
 * If using Ceedling or the test runner generator, these will be called
 * automatically if they exist.
 */

/**
 * @brief suite set up function
 *
 */
void
suiteSetUp(){};

/**
 * @brief suite tear down function
 *
 */
int
suiteTearDown(int num_failures){};

#endif // #ifdef NON_PLATFORMIO_COMPILATION

void
test_create_echo_sample_default()
{
        const Model::EchoSample sample;
        TEST_ASSERT_EQUAL_FLOAT(-1.0F, sample.distance());
}

void
test_create_echo_sample_from_sensors()
{
        Model::UltraSoundSensorPosition sensor_position(1.0F, 2.0F, 3.0F);
        Model::EchoSample               sample(sensor_position, sensor_position, 7.2F);
        TEST_ASSERT_EQUAL_FLOAT(7.2F, sample.distance());
}

void
test_receiver_getter()
{
        Model::UltraSoundSensorPosition sender(1.0F, 2.0F, 3.0F);
        Model::UltraSoundSensorPosition receiver(4.0F, 5.0F, 6.0F);

        Model::EchoSample               sample(sender, receiver, 7.2F);
        Model::UltraSoundSensorPosition sensor_position = sample.receiver();
        TEST_ASSERT_EQUAL_FLOAT(receiver[0], sensor_position[0]);
        TEST_ASSERT_EQUAL_FLOAT(receiver[1], sensor_position[1]);
        TEST_ASSERT_EQUAL_FLOAT(receiver[2], sensor_position[2]);
}

void
test_sender_getter()
{
        Model::UltraSoundSensorPosition sender(1.0F, 2.0F, 3.0F);
        Model::UltraSoundSensorPosition receiver(4.0F, 5.0F, 6.0F);

        Model::EchoSample               sample(sender, receiver, 7.2F);
        Model::UltraSoundSensorPosition sensor_position = sample.sender();
        TEST_ASSERT_EQUAL_FLOAT(sender[0], sensor_position[0]);
        TEST_ASSERT_EQUAL_FLOAT(sender[1], sensor_position[1]);
        TEST_ASSERT_EQUAL_FLOAT(sender[2], sensor_position[2]);
}

void
test_serialize()
{
        Model::UltraSoundSensorPosition position(1.0F, 2.0F, 3.0F);
        Model::EchoSample               sample(position, position, 9.0F);
        constexpr auto                  buffer_size = Model::EchoSample::serial_buffer_size;

        unsigned char *buffer = new unsigned char[buffer_size];
        buffer                = sample.serialize(buffer);

        Model::EchoSample reconstruction;
        buffer = reconstruction.deserialize(buffer - buffer_size);

        TEST_ASSERT_EQUAL_FLOAT(sample.distance(), reconstruction.distance());

        delete[](buffer - buffer_size);
}

void
process()
{
        UNITY_BEGIN();
        RUN_TEST(test_create_echo_sample_default);
        RUN_TEST(test_create_echo_sample_from_sensors);
        RUN_TEST(test_receiver_getter);
        RUN_TEST(test_sender_getter);
        RUN_TEST(test_serialize);
        UNITY_END();
}

#ifdef ARDUINO

#include <Arduino.h>
void
setup()
{
        delay(5000);
        process();
}

void
loop()
{
        digitalWrite(13, HIGH);
        delay(100);
        digitalWrite(13, LOW);
        delay(500);
}

#else

auto
main() -> int
{
        UNITY_BEGIN();
        process();
        return UNITY_END();
}

#endif // #ifdef ARDUINO
