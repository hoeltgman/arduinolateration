// SPDX-License-Identifier: MIT

auto
test_create_echo_sample_default() -> void;

auto
test_serialize() -> void;

auto
test_create_echo_sample_from_sensors() -> void;

auto
test_receiver_getter() -> void;

auto
test_sender_getter() -> void;

void
process();
