// SPDX-License-Identifier: MIT

#include "test_Serialize.hpp"

#include "Serialize.h"

#include <stddef.h>
#include <stdint.h>
#include <unity.h>

#ifdef NON_PLATFORMIO_COMPILATION

/*
 * These functions are intended to be called before and after each test.
 * If using unity directly, these will need to be provided for each test
 * executable built. If you are using the test runner generator and/or
 * Ceedling, these are optional.
 */

/**
 * @brief set up function
 *
 */
void
setUp(){};

/**
 * @brief tear down function
 *
 */
void
tearDown(){};

/*
 * These functions are intended to be called at the beginning and end of an
 * entire test suite.  suiteTearDown() is passed the number of tests that
 * failed, and its return value becomes the exit code of main(). If using
 * Unity directly, you're in charge of calling these if they are desired.
 * If using Ceedling or the test runner generator, these will be called
 * automatically if they exist.
 */

/**
 * @brief suite set up function
 *
 */
void
suiteSetUp(){};

/**
 * @brief suite tear down function
 *
 */
int
suiteTearDown(int num_failures){};

#endif // #ifdef NON_PLATFORMIO_COMPILATION

auto
test_uint8() -> void
{
        for (uint8_t value = 0; value < UINT8_MAX; value++)
        {
                // Serialization
                uint8_t *buffer = new uint8_t[1];
                buffer          = Utility::serialize(value, buffer);
                TEST_ASSERT_EQUAL(value, *(buffer - 1));

                // Deserialization
                uint8_t value_deserialized = 0;
                buffer                     = Utility::deserialize(&value_deserialized, buffer - 1);
                TEST_ASSERT_EQUAL(value, value_deserialized);

                delete[](buffer - 1);
        }
}

auto
test_multiple_uint8() -> void
{
        uint8_t *buffer = new uint8_t[UINT8_MAX];
        for (uint8_t value = 0; value < UINT8_MAX; value++)
        {
                // Serialization
                buffer = Utility::serialize(value, buffer);
                TEST_ASSERT_EQUAL(value, *(buffer - 1));
        }

        buffer = buffer - UINT8_MAX;
        for (uint8_t value = 0; value < UINT8_MAX; value++)
        {
                // Deserialization
                uint8_t value_deserialized = 0;
                buffer                     = Utility::deserialize(&value_deserialized, buffer);
                TEST_ASSERT_EQUAL(value, value_deserialized);
        }

        delete[](buffer - UINT8_MAX);
}

auto
test_float() -> void
{
        // Serialization
        uint8_t *buffer = new uint8_t[4];
        float    value  = 1.0F;
        buffer          = Utility::serialize(value, buffer);
        // Deserialization
        float value_deserialized = 0.0F;
        buffer                   = Utility::deserialize(&value_deserialized, buffer - 4);
        TEST_ASSERT_EQUAL(value, value_deserialized);
        delete[](buffer - 4);

        // Serialization
        buffer = new uint8_t[4];
        value  = 27.0F;
        buffer = Utility::serialize(value, buffer);
        // Deserialization
        value_deserialized = 0.0F;
        buffer             = Utility::deserialize(&value_deserialized, buffer - 4);
        TEST_ASSERT_EQUAL(value, value_deserialized);
        delete[](buffer - 4);

        // Serialization
        buffer = new uint8_t[4];
        value  = -31.5F;
        buffer = Utility::serialize(value, buffer);
        // Deserialization
        value_deserialized = 0.0F;
        buffer             = Utility::deserialize(&value_deserialized, buffer - 4);
        TEST_ASSERT_EQUAL(value, value_deserialized);
        delete[](buffer - 4);
}

auto
test_multiple_float() -> void
{
        constexpr uint8_t NUM_CHECKS = 100;
        uint8_t *         buffer     = new uint8_t[NUM_CHECKS * 4];
        for (uint8_t value = 0; value < NUM_CHECKS; value++)
        {
                buffer = Utility::serialize(2.0F * value, buffer);
        }

        buffer = buffer - NUM_CHECKS * 4;
        for (uint8_t value = 0; value < NUM_CHECKS; value++)
        {
                float value_deserialized = 0.0F;
                buffer                   = Utility::deserialize(&value_deserialized, buffer);
                TEST_ASSERT_EQUAL(2.0F * value, value_deserialized);
        }

        delete[](buffer - NUM_CHECKS * 4);
}

void
process()
{
        UNITY_BEGIN();
        RUN_TEST(test_uint8);
        RUN_TEST(test_multiple_uint8);
        RUN_TEST(test_float);
        RUN_TEST(test_multiple_float);
        UNITY_END();
}

#ifdef ARDUINO

#include <Arduino.h>

void
setup()
{
        delay(5000);
        process();
}

void
loop()
{
        delay(100);
}

#else

auto
main() -> int
{
        UNITY_BEGIN();
        process();
        return UNITY_END();
}

#endif // #ifdef ARDUINO