// SPDX-License-Identifier: MIT

auto
test_uint8() -> void;

auto
test_multiple_uint8() -> void;

auto
test_float() -> void;

auto
test_multiple_float() -> void;

auto
process() -> void;
