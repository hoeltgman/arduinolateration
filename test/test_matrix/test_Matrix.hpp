// SPDX-License-Identifier: MIT

void
test_matrix_size();

void
test_matrix_at_reference();

void
test_matrix_sum_2();
void
test_matrix_sum_3();

void
test_matrix_diff_2();
void
test_matrix_diff_3();

void
test_matrix_mixed_op_pm();
void
test_matrix_mixed_op_mp();

void
test_matrix_scalar_mult_matrix();
void
test_matrix_matrix_mult_scalar();

void
test_matrix_matrix_lin_comb_variant_A();
void
test_matrix_matrix_lin_comb_variant_B();
void
test_matrix_matrix_lin_comb_variant_C();
void
test_matrix_matrix_lin_comb_variant_D();
void
test_matrix_matrix_lin_comb_variant_E();
void
test_matrix_matrix_lin_comb_variant_F();

void
test_matrix_uminus();

void
test_matrix_indexing();
void
test_matrix_set();
void
test_matrix_get_row();
void
test_matrix_get_column();
void
test_matrix_get_rowrange();

void
test_matrix_matrix_mult_vector();
void
test_matrix_matrix_mult_matrix();

void
test_matrix_transpose();

void
test_matrix_rank();

void
process();
