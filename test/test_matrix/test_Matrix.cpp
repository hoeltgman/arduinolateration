// SPDX-License-Identifier: MIT

#include "test_Matrix.hpp"

#include "Matrix.h"
#include "Vector.h"

#include <unity.h>

#ifdef NON_PLATFORMIO_COMPILATION

/*
 * These functions are intended to be called before and after each test.
 * If using unity directly, these will need to be provided for each test
 * executable built. If you are using the test runner generator and/or
 * Ceedling, these are optional.
 */

/**
 * @brief set up function
 *
 */
void
setUp(){};

/**
 * @brief tear down function
 *
 */
void
tearDown(){};

/*
 * These functions are intended to be called at the beginning and end of an
 * entire test suite.  suiteTearDown() is passed the number of tests that
 * failed, and its return value becomes the exit code of main(). If using
 * Unity directly, you're in charge of calling these if they are desired.
 * If using Ceedling or the test runner generator, these will be called
 * automatically if they exist.
 */

/**
 * @brief suite set up function
 *
 */
void
suiteSetUp(){};

/**
 * @brief suite tear down function
 *
 */
int
suiteTearDown(int num_failures){};

#endif // #ifdef NON_PLATFORMIO_COMPILATION

void
test_matrix_size()
{
        LinearAlgebra::Matrix<float, 3, 2> x;
        TEST_ASSERT_EQUAL(3, x.nr());
        TEST_ASSERT_EQUAL(2, x.nc());
        TEST_ASSERT_EQUAL(6, x.numel());
}

void
test_matrix_sum_2()
{
        LinearAlgebra::Matrix<float, 3, 2> x;
        x[0] = 1.0f;
        x[1] = 2.0f;
        x[2] = 3.0f;
        x[3] = 4.0f;
        x[4] = 5.0f;
        x[5] = 6.0f;

        LinearAlgebra::Matrix<float, 3, 2> y;
        y[0] = 1.0f;
        y[1] = 2.0f;
        y[2] = 3.0f;
        y[3] = 4.0f;
        y[4] = 5.0f;
        y[5] = 6.0f;

        LinearAlgebra::Matrix<float, 3, 2> z = x + y;
        TEST_ASSERT_EQUAL_FLOAT(2.0f, z[0]);
        TEST_ASSERT_EQUAL_FLOAT(4.0f, z[1]);
        TEST_ASSERT_EQUAL_FLOAT(6.0f, z[2]);
        TEST_ASSERT_EQUAL_FLOAT(8.0f, z[3]);
        TEST_ASSERT_EQUAL_FLOAT(10.0f, z[4]);
        TEST_ASSERT_EQUAL_FLOAT(12.0f, z[5]);
}

void
test_matrix_sum_3()
{
        LinearAlgebra::Matrix<float, 3, 2> x;
        x[0] = 1.0f;
        x[1] = 2.0f;
        x[2] = 3.0f;
        x[3] = 4.0f;
        x[4] = 5.0f;
        x[5] = 6.0f;

        LinearAlgebra::Matrix<float, 3, 2> y;
        y[0] = 1.0f;
        y[1] = 2.0f;
        y[2] = 3.0f;
        y[3] = 4.0f;
        y[4] = 5.0f;
        y[5] = 6.0f;

        LinearAlgebra::Matrix<float, 3, 2> z;
        z[0] = 1.0f;
        z[1] = 2.0f;
        z[2] = 3.0f;
        z[3] = 4.0f;
        z[4] = 5.0f;
        z[5] = 6.0f;

        LinearAlgebra::Matrix<float, 3, 2> r = x + y + z;
        TEST_ASSERT_EQUAL_FLOAT(3.0f, r[0]);
        TEST_ASSERT_EQUAL_FLOAT(6.0f, r[1]);
        TEST_ASSERT_EQUAL_FLOAT(9.0f, r[2]);
        TEST_ASSERT_EQUAL_FLOAT(12.0f, r[3]);
        TEST_ASSERT_EQUAL_FLOAT(15.0f, r[4]);
        TEST_ASSERT_EQUAL_FLOAT(18.0f, r[5]);
}

void
test_matrix_diff_2()
{
        LinearAlgebra::Matrix<float, 2, 2> x;
        x[0] = 1.0f;
        x[1] = 2.0f;
        x[2] = 3.0f;
        x[3] = 4.0f;

        LinearAlgebra::Matrix<float, 2, 2> r = x - x;
        TEST_ASSERT_EQUAL_FLOAT(0.0f, r[0]);
        TEST_ASSERT_EQUAL_FLOAT(0.0f, r[1]);
        TEST_ASSERT_EQUAL_FLOAT(0.0f, r[2]);
        TEST_ASSERT_EQUAL_FLOAT(0.0f, r[3]);
}

void
test_matrix_diff_3()
{
}

void
test_matrix_mixed_op_pm()
{
}

void
test_matrix_mixed_op_mp()
{
}

void
test_matrix_scalar_mult_matrix()
{
}

void
test_matrix_matrix_mult_scalar()
{
}

void
test_matrix_matrix_lin_comb_variant_A()
{
}

void
test_matrix_matrix_lin_comb_variant_B()
{
}

void
test_matrix_matrix_lin_comb_variant_C()
{
}

void
test_matrix_matrix_lin_comb_variant_D()
{
}

void
test_matrix_matrix_lin_comb_variant_E()
{
}

void
test_matrix_matrix_lin_comb_variant_F()
{
}

void
test_matrix_uminus()
{
}

void
test_matrix_indexing()
{
        LinearAlgebra::Matrix<float, 3, 2> x;
        x[0] = 1.0f;
        x[1] = 2.0f;
        x[2] = 3.0f;
        x[3] = 4.0f;
        x[4] = 5.0f;
        x[5] = 6.0f;

        LinearAlgebra::Matrix<float, 2, 3> y;
        y[0] = 1.0f;
        y[1] = 2.0f;
        y[2] = 3.0f;
        y[3] = 4.0f;
        y[4] = 5.0f;
        y[5] = 6.0f;

        TEST_ASSERT_EQUAL_FLOAT(1.0f, x.at(1, 1));
        TEST_ASSERT_EQUAL_FLOAT(2.0f, x.at(1, 2));
        TEST_ASSERT_EQUAL_FLOAT(3.0f, x.at(2, 1));
        TEST_ASSERT_EQUAL_FLOAT(4.0f, x.at(2, 2));
        TEST_ASSERT_EQUAL_FLOAT(5.0f, x.at(3, 1));
        TEST_ASSERT_EQUAL_FLOAT(6.0f, x.at(3, 2));

        TEST_ASSERT_EQUAL_FLOAT(1.0f, y.at(1, 1));
        TEST_ASSERT_EQUAL_FLOAT(2.0f, y.at(1, 2));
        TEST_ASSERT_EQUAL_FLOAT(3.0f, y.at(1, 3));
        TEST_ASSERT_EQUAL_FLOAT(4.0f, y.at(2, 1));
        TEST_ASSERT_EQUAL_FLOAT(5.0f, y.at(2, 2));
        TEST_ASSERT_EQUAL_FLOAT(6.0f, y.at(2, 3));
}

void
test_matrix_set()
{
        LinearAlgebra::Matrix<float, 3, 2> x;
        x.set(1, 1, 1.0);
        x.set(1, 2, 2.0);
        x.set(2, 1, 3.0);
        x.set(2, 2, 4.0);
        x.set(3, 1, 5.0);
        x.set(3, 2, 6.0);

        TEST_ASSERT_EQUAL_FLOAT(1.0f, x.at(1, 1));
        TEST_ASSERT_EQUAL_FLOAT(2.0f, x.at(1, 2));
        TEST_ASSERT_EQUAL_FLOAT(3.0f, x.at(2, 1));
        TEST_ASSERT_EQUAL_FLOAT(4.0f, x.at(2, 2));
        TEST_ASSERT_EQUAL_FLOAT(5.0f, x.at(3, 1));
        TEST_ASSERT_EQUAL_FLOAT(6.0f, x.at(3, 2));
}

void
test_matrix_at_reference()
{
        LinearAlgebra::Matrix<float, 3, 2> x;
        x.at(1, 1) = 1.0;
        x.at(1, 2) = 2.0;
        x.at(2, 1) = 3.0;
        x.at(2, 2) = 4.0;
        x.at(3, 1) = 5.0;
        x.at(3, 2) = 6.0;

        TEST_ASSERT_EQUAL_FLOAT(1.0f, x.at(1, 1));
        TEST_ASSERT_EQUAL_FLOAT(2.0f, x.at(1, 2));
        TEST_ASSERT_EQUAL_FLOAT(3.0f, x.at(2, 1));
        TEST_ASSERT_EQUAL_FLOAT(4.0f, x.at(2, 2));
        TEST_ASSERT_EQUAL_FLOAT(5.0f, x.at(3, 1));
        TEST_ASSERT_EQUAL_FLOAT(6.0f, x.at(3, 2));
}

void
test_matrix_get_row()
{
        LinearAlgebra::Matrix<float, 3, 2> x;
        x.set(1, 1, 1.0);
        x.set(1, 2, 2.0);
        x.set(2, 1, 3.0);
        x.set(2, 2, 4.0);
        x.set(3, 1, 5.0);
        x.set(3, 2, 6.0);

        LinearAlgebra::Matrix<float, 1, 2> y = x.get_row(2);

        TEST_ASSERT_EQUAL(1, y.nr());
        TEST_ASSERT_EQUAL(2, y.nc());
        TEST_ASSERT_EQUAL_FLOAT(3.0f, y.at(1, 1));
        TEST_ASSERT_EQUAL_FLOAT(4.0f, y.at(1, 2));
}

void
test_matrix_get_column()
{
        LinearAlgebra::Matrix<float, 3, 2> x;
        x.set(1, 1, 1.0);
        x.set(1, 2, 2.0);
        x.set(2, 1, 3.0);
        x.set(2, 2, 4.0);
        x.set(3, 1, 5.0);
        x.set(3, 2, 6.0);

        LinearAlgebra::Matrix<float, 3, 1> y = x.get_column(2);

        TEST_ASSERT_EQUAL(3, y.nr());
        TEST_ASSERT_EQUAL(1, y.nc());
        TEST_ASSERT_EQUAL_FLOAT(2.0f, y.at(1, 1));
        TEST_ASSERT_EQUAL_FLOAT(4.0f, y.at(2, 1));
        TEST_ASSERT_EQUAL_FLOAT(6.0f, y.at(3, 1));
}

void
test_matrix_get_rowrange()
{
        LinearAlgebra::Matrix<int, 3, 2> x;
        x.set(1, 1, 1);
        x.set(1, 2, 2);
        x.set(2, 1, 3);
        x.set(2, 2, 4);
        x.set(3, 1, 5);
        x.set(3, 2, 6);

        LinearAlgebra::MatrixRowRange<2> r(2);
        LinearAlgebra::Matrix<int, 2, 2> y = x.get_rowrange(r);
        TEST_ASSERT_EQUAL(2, y.nr());
        TEST_ASSERT_EQUAL(2, y.nc());

        TEST_ASSERT_EQUAL(3, y.at(1, 1));
        TEST_ASSERT_EQUAL(4, y.at(1, 2));
        TEST_ASSERT_EQUAL(5, y.at(2, 1));
        TEST_ASSERT_EQUAL(6, y.at(2, 2));
}

void
test_matrix_get_columnrange()
{
        LinearAlgebra::Matrix<int, 3, 2> x;
        x.set(1, 1, 1);
        x.set(1, 2, 2);
        x.set(2, 1, 3);
        x.set(2, 2, 4);
        x.set(3, 1, 5);
        x.set(3, 2, 6);

        LinearAlgebra::MatrixColumnRange<1> r(2);
        LinearAlgebra::Matrix<int, 2, 1>    y = x.get_columnrange(r);
        TEST_ASSERT_EQUAL(3, y.nr());
        TEST_ASSERT_EQUAL(1, y.nc());

        TEST_ASSERT_EQUAL(2, y.at(1, 1));
        TEST_ASSERT_EQUAL(4, y.at(2, 1));
        TEST_ASSERT_EQUAL(6, y.at(3, 1));
}

void
test_matrix_matrix_mult_vector()
{
        LinearAlgebra::Matrix<float, 2, 3> A;
        A.set(1, 1, 1.0);
        A.set(1, 2, 2.0);
        A.set(1, 3, 3.0);
        A.set(2, 1, 4.0);
        A.set(2, 2, 5.0);
        A.set(2, 3, 6.0);

        LinearAlgebra::Vector<float, 3> x;
        x.set(1, 1.0);
        x.set(2, 2.0);
        x.set(3, 3.0);

        LinearAlgebra::Vector<float, 2> b = A.eval(x);

        TEST_ASSERT_EQUAL_FLOAT(14.0f, b.at(1));
        TEST_ASSERT_EQUAL_FLOAT(32.0f, b.at(2));
}

void
test_matrix_matrix_mult_matrix()
{
        LinearAlgebra::Matrix<float, 2, 3> A;
        A.set(1, 1, 1.0);
        A.set(1, 2, 2.0);
        A.set(1, 3, 3.0);
        A.set(2, 1, 4.0);
        A.set(2, 2, 5.0);
        A.set(2, 3, 6.0);

        LinearAlgebra::Matrix<float, 3, 2> B;
        B.set(1, 1, 1.0);
        B.set(1, 2, 2.0);
        B.set(2, 1, 3.0);
        B.set(2, 2, 4.0);
        B.set(3, 1, 5.0);
        B.set(3, 2, 6.0);

        LinearAlgebra::Matrix<float, 2, 2> C = A.eval(B);

        TEST_ASSERT_EQUAL_FLOAT(22.0f, C.at(1, 1));
        TEST_ASSERT_EQUAL_FLOAT(28.0f, C.at(1, 2));
        TEST_ASSERT_EQUAL_FLOAT(49.0f, C.at(2, 1));
        TEST_ASSERT_EQUAL_FLOAT(64.0f, C.at(2, 2));
}

void
test_matrix_transpose()
{
        LinearAlgebra::Matrix<float, 2, 3> A;
        A.set(1, 1, 1.0);
        A.set(1, 2, 2.0);
        A.set(1, 3, 3.0);
        A.set(2, 1, 4.0);
        A.set(2, 2, 5.0);
        A.set(2, 3, 6.0);

        LinearAlgebra::Matrix<float, 3, 2> B = A.transpose();
        TEST_ASSERT_EQUAL(3, B.nr());
        TEST_ASSERT_EQUAL(2, B.nc());
        TEST_ASSERT_EQUAL_FLOAT(1.0, B.at(1, 1));
        TEST_ASSERT_EQUAL_FLOAT(4.0, B.at(1, 2));
        TEST_ASSERT_EQUAL_FLOAT(2.0, B.at(2, 1));
        TEST_ASSERT_EQUAL_FLOAT(5.0, B.at(2, 2));
        TEST_ASSERT_EQUAL_FLOAT(3.0, B.at(3, 1));
        TEST_ASSERT_EQUAL_FLOAT(6.0, B.at(3, 2));
}

void
test_matrix_rank()
{
}

void
process()
{
        UNITY_BEGIN();
        RUN_TEST(test_matrix_size);
        RUN_TEST(test_matrix_sum_2);
        RUN_TEST(test_matrix_sum_3);
        RUN_TEST(test_matrix_diff_2);
        RUN_TEST(test_matrix_diff_3);
        RUN_TEST(test_matrix_mixed_op_pm);
        RUN_TEST(test_matrix_mixed_op_mp);
        RUN_TEST(test_matrix_scalar_mult_matrix);
        RUN_TEST(test_matrix_matrix_mult_scalar);
        RUN_TEST(test_matrix_matrix_lin_comb_variant_A);
        RUN_TEST(test_matrix_matrix_lin_comb_variant_B);
        RUN_TEST(test_matrix_matrix_lin_comb_variant_C);
        RUN_TEST(test_matrix_matrix_lin_comb_variant_D);
        RUN_TEST(test_matrix_matrix_lin_comb_variant_E);
        RUN_TEST(test_matrix_matrix_lin_comb_variant_F);
        RUN_TEST(test_matrix_uminus);
        RUN_TEST(test_matrix_indexing);
        RUN_TEST(test_matrix_at_reference);
        RUN_TEST(test_matrix_set);
        RUN_TEST(test_matrix_get_row);
        RUN_TEST(test_matrix_get_column);
        RUN_TEST(test_matrix_matrix_mult_vector);
        RUN_TEST(test_matrix_matrix_mult_matrix);
        RUN_TEST(test_matrix_transpose);
        RUN_TEST(test_matrix_rank);
        UNITY_END();
}

#ifdef ARDUINO

#include <Arduino.h>

void
setup()
{
        delay(5000);
        process();
}

void
loop()
{
        delay(100);
}

#else

auto
main() -> int
{
        UNITY_BEGIN();
        process();
        return UNITY_END();
}

#endif // #ifdef ARDUINO
