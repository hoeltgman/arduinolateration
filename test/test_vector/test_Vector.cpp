// SPDX-License-Identifier: MIT

#include "test_Vector.hpp"

#include "Vector.h"

#include <unity.h>

#ifdef NON_PLATFORMIO_COMPILATION

/*
 * These functions are intended to be called before and after each test.
 * If using unity directly, these will need to be provided for each test
 * executable built. If you are using the test runner generator and/or
 * Ceedling, these are optional.
 */

/**
 * @brief set up function
 *
 */
void
setUp(){};

/**
 * @brief tear down function
 *
 */
void
tearDown(){};

/*
 * These functions are intended to be called at the beginning and end of an
 * entire test suite.  suiteTearDown() is passed the number of tests that
 * failed, and its return value becomes the exit code of main(). If using
 * Unity directly, you're in charge of calling these if they are desired.
 * If using Ceedling or the test runner generator, these will be called
 * automatically if they exist.
 */

/**
 * @brief suite set up function
 *
 */
void
suiteSetUp(){};

/**
 * @brief suite tear down function
 *
 */
int
suiteTearDown(int num_failures){};

#endif // #ifdef NON_PLATFORMIO_COMPILATION

void
test_vector_dot()
{
        LinearAlgebra::Vector<float, 3> x;
        x[0] = 1.0f;
        x[1] = 2.0f;
        x[2] = 3.0f;

        LinearAlgebra::Vector<float, 3> y;
        y[0] = 2.0f;
        y[1] = 3.0f;
        y[2] = 4.0f;

        float dotxy = LinearAlgebra::dot<float>(x, y);
        TEST_ASSERT_EQUAL_FLOAT(20.0f, dotxy);

        dotxy = LinearAlgebra::dot<float>(x, x + y);
        TEST_ASSERT_EQUAL_FLOAT(34.0f, dotxy);

        dotxy = LinearAlgebra::dot<float>(x + y, x);
        TEST_ASSERT_EQUAL_FLOAT(34.0f, dotxy);

        dotxy = LinearAlgebra::dot<float>(x, 2.0 * y);
        TEST_ASSERT_EQUAL_FLOAT(40.0f, dotxy);
}

void
test_vector_norm()
{
        LinearAlgebra::Vector<float, 3> x;
        x[0] = 1.0f;
        x[1] = 0.0f;
        x[2] = 0.0f;

        float normx = LinearAlgebra::norm<float>(x);
        TEST_ASSERT_EQUAL_FLOAT(1.0f, normx);

        normx = LinearAlgebra::norm<float>(-x);
        TEST_ASSERT_EQUAL_FLOAT(1.0f, normx);

        normx = LinearAlgebra::norm<float>(x + x);
        TEST_ASSERT_EQUAL_FLOAT(2.0f, normx);

        normx = LinearAlgebra::norm<float>(2.0 * x);
        TEST_ASSERT_EQUAL_FLOAT(2.0f, normx);

        normx = LinearAlgebra::norm<float>(x + 3.0 * x);
        TEST_ASSERT_EQUAL_FLOAT(4.0f, normx);

        x[0]  = -2.0f;
        normx = LinearAlgebra::norm<float>(x);
        TEST_ASSERT_EQUAL_FLOAT(2.0f, normx);

        x[0]  = 3.0f;
        x[1]  = 4.0f;
        normx = LinearAlgebra::norm<float>(x);
        TEST_ASSERT_EQUAL_FLOAT(5.0f, normx);
}

void
test_vector_cross()
{
        LinearAlgebra::Vector<float, 3> x;
        x[0] = 1.0f;
        x[1] = 0.0f;
        x[2] = 0.0f;

        LinearAlgebra::Vector<float, 3> y;
        y[0] = 0.0f;
        y[1] = 1.0f;
        y[2] = 0.0f;

        LinearAlgebra::Vector<float, 3> crossxy = LinearAlgebra::cross<float>(x, y);
        TEST_ASSERT_EQUAL_FLOAT(0.0f, crossxy[0]);
        TEST_ASSERT_EQUAL_FLOAT(0.0f, crossxy[1]);
        TEST_ASSERT_EQUAL_FLOAT(1.0f, crossxy[2]);

        LinearAlgebra::Vector<float, 3> crossyx = LinearAlgebra::cross<float>(y, x);
        TEST_ASSERT_EQUAL_FLOAT(0.0f, crossyx[0]);
        TEST_ASSERT_EQUAL_FLOAT(0.0f, crossyx[1]);
        TEST_ASSERT_EQUAL_FLOAT(-1.0f, crossyx[2]);

        LinearAlgebra::Vector<float, 3> crossxx = LinearAlgebra::cross<float>(x, x);
        TEST_ASSERT_EQUAL_FLOAT(0.0f, crossxx[0]);
        TEST_ASSERT_EQUAL_FLOAT(0.0f, crossxx[1]);
        TEST_ASSERT_EQUAL_FLOAT(0.0f, crossxx[2]);
}

void
process()
{
        UNITY_BEGIN();
        RUN_TEST(test_vector_dot);
        RUN_TEST(test_vector_norm);
        RUN_TEST(test_vector_cross);
        UNITY_END();
}

#ifdef ARDUINO

#include <Arduino.h>

void
setup()
{
        delay(5000);
        process();
}

void
loop()
{
        delay(100);
}

#else

auto
main() -> int
{
        UNITY_BEGIN();
        process();
        return UNITY_END();
}

#endif // #ifdef ARDUINO
