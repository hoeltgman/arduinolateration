CREATE_UNIT_TEST (
  CPP_NAME              test_Vector.cpp
  BINARY_NAME           test_Vector_app
  COVERAGE_TARGET_NAME  test_Vector_app_coverage
  DEPENDENCIES          LinearAlgebra Unity
  INCLUDES              ${CMAKE_SOURCE_DIR}/lib/LinearAlgebra
)
