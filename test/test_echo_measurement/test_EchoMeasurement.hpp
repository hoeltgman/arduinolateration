// SPDX-License-Identifier: MIT

void
test_create_echoMeasurement();

auto
test_serialize() -> void;

auto
test_append() -> void;

auto
test_append_too_many() -> void;

void
process();
