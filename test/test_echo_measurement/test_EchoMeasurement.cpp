// SPDX-License-Identifier: MIT

#include "test_EchoMeasurement.hpp"

#include "EchoMeasurement.h"
#include "EchoSample.h"
#include "UltraSoundSensorPosition.h"

#include <unity.h>

#ifdef NON_PLATFORMIO_COMPILATION

/*
 * These functions are intended to be called before and after each test.
 * If using unity directly, these will need to be provided for each test
 * executable built. If you are using the test runner generator and/or
 * Ceedling, these are optional.
 */

/**
 * @brief set up function
 *
 */
void
setUp(){};

/**
 * @brief tear down function
 *
 */
void
tearDown(){};

/*
 * These functions are intended to be called at the beginning and end of an
 * entire test suite.  suiteTearDown() is passed the number of tests that
 * failed, and its return value becomes the exit code of main(). If using
 * Unity directly, you're in charge of calling these if they are desired.
 * If using Ceedling or the test runner generator, these will be called
 * automatically if they exist.
 */

/**
 * @brief suite set up function
 *
 */
void
suiteSetUp(){};

/**
 * @brief suite tear down function
 *
 */
int
suiteTearDown(int num_failures){};

#endif // #ifdef NON_PLATFORMIO_COMPILATION

void
test_create_echoMeasurement()
{
        Model::EchoMeasurement measurement;
        TEST_ASSERT(true);
}

void
test_serialize()
{
        Model::EchoMeasurement measurement;
        constexpr auto         buffer_size = Model::EchoMeasurement::serial_buffer_size;

        unsigned char *buffer = new unsigned char[buffer_size];
        buffer                = measurement.serialize(buffer);

        Model::EchoMeasurement reconstruction;
        buffer = reconstruction.deserialize(buffer - buffer_size);

        TEST_ASSERT_GREATER_OR_EQUAL(0, buffer_size);

        delete[](buffer - buffer_size);
}

void
test_append()
{
        Model::EchoMeasurement          measurement;
        Model::UltraSoundSensorPosition position(1.0F, 2.0F, 3.0F);
        Model::EchoSample               sample(position, position, 9.0F);
        measurement.append(sample);
        TEST_ASSERT(true);
}

void
test_append_too_many()
{
        Model::EchoMeasurement          measurement;
        Model::UltraSoundSensorPosition position(1.0F, 2.0F, 3.0F);
        Model::EchoSample               sample(position, position, 9.0F);
        for (auto idx = 0; idx < measurement.MAX_NUM_SAMPLES + 1; idx++)
        {
                measurement.append(sample);
        }
        TEST_ASSERT(true);
}

void
process()
{
        UNITY_BEGIN();
        RUN_TEST(test_create_echoMeasurement);
        RUN_TEST(test_serialize);
        RUN_TEST(test_append);
        RUN_TEST(test_append_too_many);
        UNITY_END();
}

#ifdef ARDUINO

#include <Arduino.h>
void
setup()
{
        delay(5000);
        process();
}

void
loop()
{
        delay(100);
}

#else

auto
main() -> int
{
        UNITY_BEGIN();
        process();
        return UNITY_END();
}

#endif // #ifdef ARDUINO
