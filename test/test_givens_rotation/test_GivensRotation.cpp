// SPDX-License-Identifier: MIT

#include "test_GivensRotation.hpp"

#include "Array.h"
#include "GivensRotation.h"

#include <cmath>
#include <unity.h>

#ifdef NON_PLATFORMIO_COMPILATION

/*
 * These functions are intended to be called before and after each test.
 * If using unity directly, these will need to be provided for each test
 * executable built. If you are using the test runner generator and/or
 * Ceedling, these are optional.
 */

/**
 * @brief set up function
 *
 */
void
setUp(){};

/**
 * @brief tear down function
 *
 */
void
tearDown(){};

/*
 * These functions are intended to be called at the beginning and end of an
 * entire test suite.  suiteTearDown() is passed the number of tests that
 * failed, and its return value becomes the exit code of main(). If using
 * Unity directly, you're in charge of calling these if they are desired.
 * If using Ceedling or the test runner generator, these will be called
 * automatically if they exist.
 */

/**
 * @brief suite set up function
 *
 */
void
suiteSetUp(){};

/**
 * @brief suite tear down function
 *
 */
int
suiteTearDown(int num_failures){};

#endif // #ifdef NON_PLATFORMIO_COMPILATION

void
test_row_index_getter()
{
        size_t                               expected_row_index    = 4;
        size_t                               expected_column_index = 3;
        float                                expected_angle        = 0.72f;
        LinearAlgebra::GivensRotation<float> gr(expected_row_index, expected_column_index, expected_angle);
        size_t                               actual_row_index = gr.row_index();
        TEST_ASSERT_EQUAL(expected_row_index, actual_row_index);
}

void
test_column_index_getter()
{
        size_t                               expected_row_index    = 4;
        size_t                               expected_column_index = 3;
        float                                expected_angle        = 0.72f;
        LinearAlgebra::GivensRotation<float> gr(expected_row_index, expected_column_index, expected_angle);
        size_t                               actual_column_index = gr.column_index();
        TEST_ASSERT_EQUAL(expected_column_index, actual_column_index);
}

void
test_angle_getter()
{
        size_t                               expected_row_index    = 4;
        size_t                               expected_column_index = 3;
        float                                expected_angle        = 0.72f;
        LinearAlgebra::GivensRotation<float> gr(expected_row_index, expected_column_index, expected_angle);
        float                                actual_angle = gr.angle();
        TEST_ASSERT_EQUAL_FLOAT(expected_angle, actual_angle);
}

void
test_at()
{
        size_t                               row_index    = 4;
        size_t                               column_index = 3;
        float                                angle        = 0.72f;
        LinearAlgebra::GivensRotation<float> gr(row_index, column_index, angle);
        float                                expected_cosine = cos(angle);
        float                                expected_sine   = sin(angle);
        TEST_ASSERT_EQUAL_FLOAT(expected_cosine, gr.at(row_index, row_index));
        TEST_ASSERT_EQUAL_FLOAT(expected_cosine, gr.at(column_index, column_index));
        TEST_ASSERT_EQUAL_FLOAT(expected_sine, gr.at(row_index, column_index));
        TEST_ASSERT_EQUAL_FLOAT(-expected_sine, gr.at(column_index, row_index));
        TEST_ASSERT_EQUAL_FLOAT(1.0f, gr.at(2, 2));
        TEST_ASSERT_EQUAL_FLOAT(0.0f, gr.at(9, 2));
}

void
test_eval_A()
{
        size_t                               row_index    = 4;
        size_t                               column_index = 3;
        float                                angle        = 0.72f;
        LinearAlgebra::GivensRotation<float> gr(row_index, column_index, angle);
        LinearAlgebra::Array<float, 4>       in_vector;

        in_vector[0]                              = 0.0f;
        in_vector[1]                              = 0.0f;
        in_vector[2]                              = 1.0f;
        in_vector[3]                              = 1.0f;
        LinearAlgebra::Array<float, 4> out_vector = gr.eval<4>(in_vector);
        TEST_ASSERT_EQUAL_FLOAT(0.0f, out_vector[0]);
        TEST_ASSERT_EQUAL_FLOAT(0.0f, out_vector[1]);
        TEST_ASSERT_EQUAL_FLOAT(cos(angle) - sin(angle), out_vector[2]);
        TEST_ASSERT_EQUAL_FLOAT(cos(angle) + sin(angle), out_vector[3]);
}

void
test_eval_B()
{
        size_t                               row_index    = 3;
        size_t                               column_index = 4;
        float                                angle        = 0.72f;
        LinearAlgebra::GivensRotation<float> gr(row_index, column_index, angle);
        LinearAlgebra::Array<float, 4>       in_vector;

        in_vector[0]                              = 0.0f;
        in_vector[1]                              = 0.0f;
        in_vector[2]                              = 1.0f;
        in_vector[3]                              = 1.0f;
        LinearAlgebra::Array<float, 4> out_vector = gr.eval<4>(in_vector);
        TEST_ASSERT_EQUAL_FLOAT(0.0f, out_vector[0]);
        TEST_ASSERT_EQUAL_FLOAT(0.0f, out_vector[1]);
        TEST_ASSERT_EQUAL_FLOAT(cos(angle) + sin(angle), out_vector[2]);
        TEST_ASSERT_EQUAL_FLOAT(cos(angle) - sin(angle), out_vector[3]);
}

void
process()
{
        UNITY_BEGIN();
        RUN_TEST(test_row_index_getter);
        RUN_TEST(test_column_index_getter);
        RUN_TEST(test_angle_getter);
        RUN_TEST(test_at);
        RUN_TEST(test_eval_A);
        RUN_TEST(test_eval_B);
        UNITY_END();
}

#ifdef ARDUINO

#include <Arduino.h>

void
setup()
{
        delay(5000);
        process();
}

void
loop()
{
        delay(100);
}

#else

auto
main() -> int
{
        UNITY_BEGIN();
        process();
        return UNITY_END();
}

#endif // #ifdef ARDUINO