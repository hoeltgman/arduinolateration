// SPDX-License-Identifier: MIT

#include "test_Array.hpp"

#include "Array.h"

#include <unity.h>

#ifdef NON_PLATFORMIO_COMPILATION

/*
 * These functions are intended to be called before and after each test.
 * If using unity directly, these will need to be provided for each test
 * executable built. If you are using the test runner generator and/or
 * Ceedling, these are optional.
 */

/**
 * @brief set up function
 *
 */
void
setUp(){};

/**
 * @brief tear down function
 *
 */
void
tearDown(){};

/*
 * These functions are intended to be called at the beginning and end of an
 * entire test suite.  suiteTearDown() is passed the number of tests that
 * failed, and its return value becomes the exit code of main(). If using
 * Unity directly, you're in charge of calling these if they are desired.
 * If using Ceedling or the test runner generator, these will be called
 * automatically if they exist.
 */

/**
 * @brief suite set up function
 *
 */
void
suiteSetUp(){};

/**
 * @brief suite tear down function
 *
 */
int
suiteTearDown(int num_failures){};

#endif // #ifdef NON_PLATFORMIO_COMPILATION

void
test_array_size()
{
        LinearAlgebra::Array<float, 3> x;
        TEST_ASSERT_EQUAL(3, x.numel());
}

void
test_array_uminus()
{
        LinearAlgebra::Array<float, 3> x;
        x[0] = 1.0f;
        x[1] = 2.0f;
        x[2] = 3.0f;

        LinearAlgebra::Array<float, 3> r = -x;

        TEST_ASSERT_EQUAL_FLOAT(-1.0f, r[0]);
        TEST_ASSERT_EQUAL_FLOAT(-2.0f, r[1]);
        TEST_ASSERT_EQUAL_FLOAT(-3.0f, r[2]);
}

void
test_array_sum_2()
{
        LinearAlgebra::Array<float, 3> x;
        x[0] = 1.0f;
        x[1] = 2.0f;
        x[2] = 3.0f;

        LinearAlgebra::Array<float, 3> y;
        y[0] = 1.0f;
        y[1] = 4.0f;
        y[2] = 6.0f;

        LinearAlgebra::Array<float, 3> z = x + y;

        TEST_ASSERT_EQUAL_FLOAT(2.0f, z[0]);
        TEST_ASSERT_EQUAL_FLOAT(6.0f, z[1]);
        TEST_ASSERT_EQUAL_FLOAT(9.0f, z[2]);
}

void
test_array_sum_3()
{
        LinearAlgebra::Array<float, 3> x;
        x[0] = 1.0f;
        x[1] = 2.0f;
        x[2] = 3.0f;

        LinearAlgebra::Array<float, 3> y;
        y[0] = 1.0f;
        y[1] = 4.0f;
        y[2] = 6.0f;

        LinearAlgebra::Array<float, 3> z;
        z[0] = 1.0f;
        z[1] = 1.0f;
        z[2] = 0.0f;

        LinearAlgebra::Array<float, 3> r = x + y + z;

        TEST_ASSERT_EQUAL_FLOAT(3.0f, r[0]);
        TEST_ASSERT_EQUAL_FLOAT(7.0f, r[1]);
        TEST_ASSERT_EQUAL_FLOAT(9.0f, r[2]);
}

void
test_array_diff_2()
{
        LinearAlgebra::Array<float, 3> x;
        x[0] = 1.0f;
        x[1] = 2.0f;
        x[2] = 3.0f;

        LinearAlgebra::Array<float, 3> y;
        y[0] = 1.0f;
        y[1] = 4.0f;
        y[2] = 6.0f;

        LinearAlgebra::Array<float, 3> z = x - y;

        TEST_ASSERT_EQUAL_FLOAT(0.0f, z[0]);
        TEST_ASSERT_EQUAL_FLOAT(-2.0f, z[1]);
        TEST_ASSERT_EQUAL_FLOAT(-3.0f, z[2]);
}

void
test_array_diff_3()
{
        LinearAlgebra::Array<float, 3> x;
        x[0] = 1.0f;
        x[1] = 2.0f;
        x[2] = 3.0f;

        LinearAlgebra::Array<float, 3> y;
        y[0] = 1.0f;
        y[1] = 4.0f;
        y[2] = 6.0f;

        LinearAlgebra::Array<float, 3> z;
        z[0] = 1.0f;
        z[1] = 1.0f;
        z[2] = 0.0f;

        LinearAlgebra::Array<float, 3> r = x - y - z;

        TEST_ASSERT_EQUAL_FLOAT(-1.0f, r[0]);
        TEST_ASSERT_EQUAL_FLOAT(-3.0f, r[1]);
        TEST_ASSERT_EQUAL_FLOAT(-3.0f, r[2]);
}

void
test_array_mixed_op_pm()
{
        LinearAlgebra::Array<float, 3> x;
        x[0] = 1.0f;
        x[1] = 2.0f;
        x[2] = 3.0f;

        LinearAlgebra::Array<float, 3> y;
        y[0] = 1.0f;
        y[1] = 4.0f;
        y[2] = 6.0f;

        LinearAlgebra::Array<float, 3> z;
        z[0] = 1.0f;
        z[1] = 1.0f;
        z[2] = 0.0f;

        LinearAlgebra::Array<float, 3> r = x + y - z;

        TEST_ASSERT_EQUAL_FLOAT(1.0f, r[0]);
        TEST_ASSERT_EQUAL_FLOAT(5.0f, r[1]);
        TEST_ASSERT_EQUAL_FLOAT(9.0f, r[2]);
}

void
test_array_mixed_op_mp()
{
        LinearAlgebra::Array<float, 3> x;
        x[0] = 1.0f;
        x[1] = 2.0f;
        x[2] = 3.0f;

        LinearAlgebra::Array<float, 3> y;
        y[0] = 1.0f;
        y[1] = 4.0f;
        y[2] = 6.0f;

        LinearAlgebra::Array<float, 3> z;
        z[0] = 1.0f;
        z[1] = 1.0f;
        z[2] = 0.0f;

        LinearAlgebra::Array<float, 3> r = x - y + z;

        TEST_ASSERT_EQUAL_FLOAT(1.0f, r[0]);
        TEST_ASSERT_EQUAL_FLOAT(-1.0f, r[1]);
        TEST_ASSERT_EQUAL_FLOAT(-3.0f, r[2]);
}

void
test_array_scalar_mult_array()
{
        LinearAlgebra::Array<float, 3> x;
        x[0] = 1.0f;
        x[1] = 2.0f;
        x[2] = 3.0f;

        float alpha = 2.0;

        LinearAlgebra::Array<float, 3> r = alpha * x;

        TEST_ASSERT_EQUAL_FLOAT(2.0f, r[0]);
        TEST_ASSERT_EQUAL_FLOAT(4.0f, r[1]);
        TEST_ASSERT_EQUAL_FLOAT(6.0f, r[2]);
}

void
test_array_array_mult_scalar()
{
        LinearAlgebra::Array<float, 3> x;
        x[0] = 1.0f;
        x[1] = 2.0f;
        x[2] = 3.0f;

        float alpha = 2.0;

        LinearAlgebra::Array<float, 3> r = x * alpha;

        TEST_ASSERT_EQUAL_FLOAT(2.0f, r[0]);
        TEST_ASSERT_EQUAL_FLOAT(4.0f, r[1]);
        TEST_ASSERT_EQUAL_FLOAT(6.0f, r[2]);
}

void
test_array_array_lin_comb_variant_A()
{
        LinearAlgebra::Array<float, 3> x;
        x[0] = 1.0f;
        x[1] = 2.0f;
        x[2] = 3.0f;
        LinearAlgebra::Array<float, 3> y;
        y[0] = 1.0f;
        y[1] = 2.0f;
        y[2] = 3.0f;

        float alpha = 2.0;

        LinearAlgebra::Array<float, 3> r = alpha * x + y;

        TEST_ASSERT_EQUAL_FLOAT(3.0f, r[0]);
        TEST_ASSERT_EQUAL_FLOAT(6.0f, r[1]);
        TEST_ASSERT_EQUAL_FLOAT(9.0f, r[2]);
}

void
test_array_array_lin_comb_variant_B()
{
        LinearAlgebra::Array<float, 3> x;
        x[0] = 1.0f;
        x[1] = 2.0f;
        x[2] = 3.0f;

        LinearAlgebra::Array<float, 3> y;
        y[0] = 1.0f;
        y[1] = 2.0f;
        y[2] = 3.0f;

        float alpha = 2.0;

        LinearAlgebra::Array<float, 3> r = y + alpha * x;

        TEST_ASSERT_EQUAL_FLOAT(3.0f, r[0]);
        TEST_ASSERT_EQUAL_FLOAT(6.0f, r[1]);
        TEST_ASSERT_EQUAL_FLOAT(9.0f, r[2]);
}

void
test_array_array_lin_comb_variant_C()
{
        LinearAlgebra::Array<float, 3> x;
        x[0] = 1.0f;
        x[1] = 2.0f;
        x[2] = 3.0f;

        LinearAlgebra::Array<float, 3> y;
        y[0] = 1.0f;
        y[1] = 2.0f;
        y[2] = 3.0f;

        float alpha = 2.0;

        LinearAlgebra::Array<float, 3> r = x * alpha + y;

        TEST_ASSERT_EQUAL_FLOAT(3.0f, r[0]);
        TEST_ASSERT_EQUAL_FLOAT(6.0f, r[1]);
        TEST_ASSERT_EQUAL_FLOAT(9.0f, r[2]);
}

void
test_array_array_lin_comb_variant_D()
{
        LinearAlgebra::Array<float, 3> x;
        x[0] = 1.0f;
        x[1] = 2.0f;
        x[2] = 3.0f;

        LinearAlgebra::Array<float, 3> y;
        y[0] = 1.0f;
        y[1] = 2.0f;
        y[2] = 3.0f;

        float alpha = 2.0;

        LinearAlgebra::Array<float, 3> r = y + x * alpha;

        TEST_ASSERT_EQUAL_FLOAT(3.0f, r[0]);
        TEST_ASSERT_EQUAL_FLOAT(6.0f, r[1]);
        TEST_ASSERT_EQUAL_FLOAT(9.0f, r[2]);
}

void
test_array_array_lin_comb_variant_E()
{
        LinearAlgebra::Array<float, 3> x;
        x[0] = 1.0f;
        x[1] = 2.0f;
        x[2] = 3.0f;

        LinearAlgebra::Array<float, 3> y;
        y[0] = 1.0f;
        y[1] = 2.0f;
        y[2] = 3.0f;

        float alpha = 2.0;

        LinearAlgebra::Array<float, 3> r = (y + x) * alpha;

        TEST_ASSERT_EQUAL_FLOAT(4.0f, r[0]);
        TEST_ASSERT_EQUAL_FLOAT(8.0f, r[1]);
        TEST_ASSERT_EQUAL_FLOAT(12.0f, r[2]);
}

void
test_array_array_lin_comb_variant_F()
{
        LinearAlgebra::Array<float, 3> x;
        x[0] = 1.0f;
        x[1] = 2.0f;
        x[2] = 3.0f;

        LinearAlgebra::Array<float, 3> y;
        y[0] = 1.0f;
        y[1] = 2.0f;
        y[2] = 3.0f;

        float alpha = 2.0;

        LinearAlgebra::Array<float, 3> r = alpha * (y + x);

        TEST_ASSERT_EQUAL_FLOAT(4.0f, r[0]);
        TEST_ASSERT_EQUAL_FLOAT(8.0f, r[1]);
        TEST_ASSERT_EQUAL_FLOAT(12.0f, r[2]);
}

void
test_array_indexing()
{
}

void
process()
{
        UNITY_BEGIN();
        RUN_TEST(test_array_size);
        RUN_TEST(test_array_uminus);
        RUN_TEST(test_array_sum_2);
        RUN_TEST(test_array_sum_3);
        RUN_TEST(test_array_diff_2);
        RUN_TEST(test_array_diff_3);
        RUN_TEST(test_array_mixed_op_pm);
        RUN_TEST(test_array_mixed_op_mp);
        RUN_TEST(test_array_scalar_mult_array);
        RUN_TEST(test_array_array_mult_scalar);
        RUN_TEST(test_array_array_lin_comb_variant_A);
        RUN_TEST(test_array_array_lin_comb_variant_B);
        RUN_TEST(test_array_array_lin_comb_variant_C);
        RUN_TEST(test_array_array_lin_comb_variant_D);
        RUN_TEST(test_array_array_lin_comb_variant_E);
        RUN_TEST(test_array_array_lin_comb_variant_F);
        RUN_TEST(test_array_indexing);
        UNITY_END();
}

#ifdef ARDUINO

#include <Arduino.h>

void
setup()
{
        delay(5000);
        process();
}

void
loop()
{
        delay(100);
}

#else

auto
main() -> int
{
        UNITY_BEGIN();
        process();
        return UNITY_END();
}

#endif // #ifdef ARDUINO
