CREATE_UNIT_TEST (
  CPP_NAME              test_Array.cpp
  BINARY_NAME           test_Array_app
  COVERAGE_TARGET_NAME  test_Array_app_coverage
  DEPENDENCIES          LinearAlgebra Unity
  INCLUDES              ${CMAKE_SOURCE_DIR}/lib/LinearAlgebra
)
