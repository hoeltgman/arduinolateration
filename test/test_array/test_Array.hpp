// SPDX-License-Identifier: MIT

void
test_array_size();

void
test_array_uminus();

void
test_array_sum_2();
void
test_array_sum_3();

void
test_array_diff_2();
void
test_array_diff_3();

void
test_array_mixed_op_pm();
void
test_array_mixed_op_mp();

void
test_array_scalar_mult_array();
void
test_array_array_mult_scalar();

void
test_array_array_lin_comb_variant_A();
void
test_array_array_lin_comb_variant_B();
void
test_array_array_lin_comb_variant_C();
void
test_array_array_lin_comb_variant_D();
void
test_array_array_lin_comb_variant_E();
void
test_array_array_lin_comb_variant_F();

void
test_array_indexing();

void
process();
