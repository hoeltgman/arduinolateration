// SPDX-License-Identifier: MIT

#include "test_UltraSoundSensor.hpp"

#include "UltraSoundSensor.h"
#include "UltraSoundSensorPosition.h"

#include <unity.h>

#ifdef NON_PLATFORMIO_COMPILATION

/*
 * These functions are intended to be called before and after each test.
 * If using unity directly, these will need to be provided for each test
 * executable built. If you are using the test runner generator and/or
 * Ceedling, these are optional.
 */

/**
 * @brief set up function
 *
 */
void
setUp(){};

/**
 * @brief tear down function
 *
 */
void
tearDown(){};

/*
 * These functions are intended to be called at the beginning and end of an
 * entire test suite.  suiteTearDown() is passed the number of tests that
 * failed, and its return value becomes the exit code of main(). If using
 * Unity directly, you're in charge of calling these if they are desired.
 * If using Ceedling or the test runner generator, these will be called
 * automatically if they exist.
 */

/**
 * @brief suite set up function
 *
 */
void
suiteSetUp(){};

/**
 * @brief suite tear down function
 *
 */
int
suiteTearDown(int num_failures){};

#endif // #ifdef NON_PLATFORMIO_COMPILATION

void
test_ultrasoundsensor_sensor_id()
{
        Model::UltraSoundSensorPosition pos{1.0f, 2.0f, 3.0f};
        uint8_t                         trig_pin = 1;
        uint8_t                         echo_pin = 2;
        float                           distance = 1.0f;
        Model::UltraSoundSensor         uss(pos, trig_pin, echo_pin, distance);
        uint8_t                         id_expected = 17;
        uint8_t                         id_sensor   = uss.sensor_id();

        TEST_ASSERT_EQUAL(id_expected, id_sensor);
}

void
test_ultrasoundsensor_position()
{
        Model::UltraSoundSensorPosition pos{1.0f, 2.0f, 3.0f};
        uint8_t                         trig_pin = 1;
        uint8_t                         echo_pin = 2;
        float                           distance = 1.0f;
        Model::UltraSoundSensor         uss(pos, trig_pin, echo_pin, distance);
        Model::UltraSoundSensorPosition pos_result = uss.position();

        TEST_ASSERT_EQUAL_FLOAT(pos[0], pos_result[0]);
        TEST_ASSERT_EQUAL_FLOAT(pos[1], pos_result[1]);
        TEST_ASSERT_EQUAL_FLOAT(pos[2], pos_result[2]);
}

void
test_reset_sensor()
{
        Model::UltraSoundSensorPosition pos{1.0f, 2.0f, 3.0f};
        uint8_t                         trig_pin = 1;
        uint8_t                         echo_pin = 2;
        float                           distance = 1.0f;
        Model::UltraSoundSensor         uss(pos, trig_pin, echo_pin, distance);
        uss.reset_sensor(); // Reset does nothing at the moment.
        TEST_ASSERT(true);
}

void
process()
{
        UNITY_BEGIN();
        RUN_TEST(test_ultrasoundsensor_sensor_id);
        RUN_TEST(test_ultrasoundsensor_position);
        RUN_TEST(test_reset_sensor);
        UNITY_END();
}

#ifdef ARDUINO

#include <Arduino.h>
void
setup()
{
        delay(5000);
        process();
}

void
loop()
{
        delay(100);
}

#else

auto
main() -> int
{
        UNITY_BEGIN();
        process();
        return UNITY_END();
}

#endif // #ifdef ARDUINO
