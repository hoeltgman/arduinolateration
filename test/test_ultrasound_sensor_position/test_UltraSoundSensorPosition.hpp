// SPDX-License-Identifier: MIT

void
test_create_ultrasound_sensor_position_default();

void
test_create_ultrasound_sensor_position_xyz();

void
test_create_ultrasound_sensor_position_index();

void
test_create_ultrasound_sensor_position_index_mod();

auto
test_serialize() -> void;

auto
test_serialize_mult() -> void;

void
process();
