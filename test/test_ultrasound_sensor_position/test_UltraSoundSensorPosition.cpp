// SPDX-License-Identifier: MIT

#include "test_UltraSoundSensorPosition.hpp"

#include "UltraSoundSensorPosition.h"

#include <stddef.h>
#include <stdint.h>
#include <unity.h>

#ifdef NON_PLATFORMIO_COMPILATION

/*
 * These functions are intended to be called before and after each test.
 * If using unity directly, these will need to be provided for each test
 * executable built. If you are using the test runner generator and/or
 * Ceedling, these are optional.
 */

/**
 * @brief set up function
 *
 */
void
setUp(){};

/**
 * @brief tear down function
 *
 */
void
tearDown(){};

/*
 * These functions are intended to be called at the beginning and end of an
 * entire test suite.  suiteTearDown() is passed the number of tests that
 * failed, and its return value becomes the exit code of main(). If using
 * Unity directly, you're in charge of calling these if they are desired.
 * If using Ceedling or the test runner generator, these will be called
 * automatically if they exist.
 */

/**
 * @brief suite set up function
 *
 */
void
suiteSetUp(){};

/**
 * @brief suite tear down function
 *
 */
int
suiteTearDown(int num_failures){};

#endif // #ifdef NON_PLATFORMIO_COMPILATION

void
test_create_ultrasound_sensor_position_default()
{
        const Model::UltraSoundSensorPosition position;
        TEST_ASSERT_EQUAL_FLOAT(0.0F, position.x());
        TEST_ASSERT_EQUAL_FLOAT(0.0F, position.y());
        TEST_ASSERT_EQUAL_FLOAT(0.0F, position.z());
}

void
test_create_ultrasound_sensor_position_xyz()
{
        const Model::UltraSoundSensorPosition position(1.0F, 2.0F, 3.0F);
        TEST_ASSERT_EQUAL_FLOAT(1.0F, position.x());
        TEST_ASSERT_EQUAL_FLOAT(2.0F, position.y());
        TEST_ASSERT_EQUAL_FLOAT(3.0F, position.z());
}

void
test_create_ultrasound_sensor_position_index()
{
        const Model::UltraSoundSensorPosition position(1.0F, 2.0F, 3.0F);
        TEST_ASSERT_EQUAL_FLOAT(1.0F, position[0]);
        TEST_ASSERT_EQUAL_FLOAT(2.0F, position[1]);
        TEST_ASSERT_EQUAL_FLOAT(3.0F, position[2]);
}

void
test_create_ultrasound_sensor_position_index_mod()
{
        const Model::UltraSoundSensorPosition position(1.0F, 2.0F, 3.0F);
        TEST_ASSERT_EQUAL_FLOAT(1.0F, position[3]);
        TEST_ASSERT_EQUAL_FLOAT(2.0F, position[4]);
        TEST_ASSERT_EQUAL_FLOAT(3.0F, position[5]);
}

void
test_serialize()
{
        const Model::UltraSoundSensorPosition position(1.0F, 2.0F, 3.0F);
        constexpr auto                        buffer_size = Model::UltraSoundSensorPosition::serial_buffer_size;
        unsigned char                        *buffer      = new unsigned char[buffer_size];

        buffer = position.serialize(buffer);

        Model::UltraSoundSensorPosition reconstruction;
        buffer = reconstruction.deserialize(buffer - buffer_size);

        delete[](buffer - buffer_size);
        TEST_ASSERT_EQUAL_FLOAT(position.x(), reconstruction.x());
        TEST_ASSERT_EQUAL_FLOAT(position.y(), reconstruction.y());
        TEST_ASSERT_EQUAL_FLOAT(position.z(), reconstruction.z());
}

void
test_serialize_mult()
{
        const Model::UltraSoundSensorPosition position(1.0F, 2.0F, 3.0F);
        constexpr auto                        buffer_size = Model::UltraSoundSensorPosition::serial_buffer_size;
        unsigned char                        *buffer      = new unsigned char[2 * buffer_size];

        buffer = position.serialize(buffer);
        buffer = position.serialize(buffer);

        Model::UltraSoundSensorPosition reconstruction, reconstruction2;
        buffer = reconstruction.deserialize(buffer - 2 * buffer_size);

        TEST_ASSERT_EQUAL_FLOAT(position.x(), reconstruction.x());
        TEST_ASSERT_EQUAL_FLOAT(position.y(), reconstruction.y());
        TEST_ASSERT_EQUAL_FLOAT(position.z(), reconstruction.z());

        buffer = reconstruction2.deserialize(buffer);

        delete[](buffer - 2 * buffer_size);
}

void
process()
{
        UNITY_BEGIN();
        RUN_TEST(test_create_ultrasound_sensor_position_default);
        RUN_TEST(test_create_ultrasound_sensor_position_xyz);
        RUN_TEST(test_create_ultrasound_sensor_position_index);
        RUN_TEST(test_create_ultrasound_sensor_position_index_mod);
        RUN_TEST(test_serialize);
        RUN_TEST(test_serialize_mult);
        UNITY_END();
}

#ifdef ARDUINO

#include <Arduino.h>
void
setup()
{
        delay(5000);
        process();
}

void
loop()
{
        digitalWrite(13, HIGH);
        delay(100);
        digitalWrite(13, LOW);
        delay(500);
}

#else

auto
main() -> int
{
        UNITY_BEGIN();
        process();
        return UNITY_END();
}

#endif // #ifdef ARDUINO
