macro ( PREVENT_IN_SOURCE_BUILDS )
if ( ${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR} )
  message ( "###############################################################" )
  message ( "# In-source builds not allowed.                               #" )
  message ( "# Please make a new build directory and run CMake from there. #" )
  message ( "# You may need to remove CMakeCache.txt.                      #" )
  message ( "###############################################################" )
  message ( FATAL_ERROR "Quitting configuration" )
endif ( ${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR} )
endmacro ( PREVENT_IN_SOURCE_BUILDS )
