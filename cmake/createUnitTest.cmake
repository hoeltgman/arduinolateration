# CREATE_UNIT_TEST (
#   CPP_NAME              foo.cpp               # name of the source file
#   BINARY_NAME           test_foo              # name of the test binary
#   COVERAGE_TARGET_NAME  test_foo_coverage     # target name for coverage
#   DEPENDENCIES          fooDep barDep         # target names of dependencies
#   INCLUDES              src/fooInc src/barInc # Folders with includes
# )
#
function ( CREATE_UNIT_TEST )

  set ( oneValueArgs CPP_NAME BINARY_NAME COVERAGE_TARGET_NAME )
  set ( multiValueArgs DEPENDENCIES INCLUDES )
  cmake_parse_arguments ( unittest "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  add_executable ( ${unittest_BINARY_NAME} ${unittest_CPP_NAME} )

  target_compile_options ( ${unittest_BINARY_NAME}
    # Debugging Flags Visual Studio
    PUBLIC "$<$<AND:$<CONFIG:Debug>,$<CXX_COMPILER_ID:MSVC>>:/Wall;/WX>"
    # Release Flags Visual Studio
    PUBLIC "$<$<AND:$<CONFIG:Release>,$<CXX_COMPILER_ID:MSVC>>:/W3>"
    # Debugging Flags gcc and clang (common flags)
    PUBLIC "$<$<AND:$<CONFIG:Debug>,$<CXX_COMPILER_ID:GNU,Clang,AppleClang>>:-Wall;-Wextra;-pedantic;-pedantic-errors>"
    # Release Flags gcc and clang (common flags)
    PUBLIC "$<$<AND:$<CONFIG:Release>,$<CXX_COMPILER_ID:GNU,Clang,AppleClang>>:-Wall>"
  )

  target_include_directories ( ${unittest_BINARY_NAME}
    PUBLIC ${unittest_INCLUDES}
  )

  target_link_libraries( ${unittest_BINARY_NAME}
    PUBLIC ${unittest_DEPENDENCIES}
    # Enable sanitizers for gcc
    PUBLIC "$<$<AND:$<CONFIG:Debug>,$<BOOL:${ENABLE_SANITIZERS}>,$<CXX_COMPILER_ID:GNU>>:-fsanitize=address;-fsanitize=pointer-compare;-fsanitize=leak;-fsanitize=undefined;-fno-omit-frame-pointer>"
    # Enable code coverage for gcc
    PUBLIC "$<$<AND:$<CONFIG:Debug>,$<BOOL:${ENABLE_COVERAGE}>,$<CXX_COMPILER_ID:GNU>>:--coverage>"
    # Enable sanitizers for clang
    PUBLIC "$<$<AND:$<CONFIG:Debug>,$<BOOL:${ENABLE_SANITIZERS}>,$<CXX_COMPILER_ID:Clang,AppleClang>>:-fsanitize=address;-fsanitize=leak;-fsanitize=leak;-fsanitize=undefined;-fno-omit-frame-pointer>"
    # Enable memory sanitizer for clang
    PUBLIC "$<$<AND:$<CONFIG:Debug>,$<BOOL:${ENABLE_MEMORY_SANITIZER}>,$<CXX_COMPILER_ID:Clang,AppleClang>>:-fsanitize=memory;-fno-omit-frame-pointer>"
    # Enable code coverage for clang
    PUBLIC "$<$<AND:$<CONFIG:Debug>,$<BOOL:${ENABLE_COVERAGE}>,$<CXX_COMPILER_ID:Clang,AppleClang>>:-fprofile-instr-generate>"
  )

  include ( codeCoverage )
  if ( ENABLE_COVERAGE )
    setup_target_for_coverage_gcovr_xml (
      NAME         ${unittest_COVERAGE_TARGET_NAME}
      EXECUTABLE   ${unittest_BINARY_NAME}
      DEPENDENCIES ${unittest_DEPENDENCIES}
    )
  endif ( ENABLE_COVERAGE )

  add_test ( NAME    ${unittest_BINARY_NAME}
             COMMAND ${unittest_BINARY_NAME}
  )

endfunction ( CREATE_UNIT_TEST )
