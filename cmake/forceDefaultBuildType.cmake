macro ( FORCE_DEFAULT_BUILD_TYPE )
# Set a default build type if none was specified
# We only use Debug and Release
set ( default_build_type "Release" )
if ( EXISTS "${CMAKE_SOURCE_DIR}/.git" )
  set (default_build_type "Debug" )
endif ( EXISTS "${CMAKE_SOURCE_DIR}/.git" )

if ( NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES )
  message ( STATUS "##########################################################" )
  message ( STATUS "# Setting build type to '${default_build_type}' as none was specified. #" )
  message ( STATUS "##########################################################" )
  set ( CMAKE_BUILD_TYPE "${default_build_type}" CACHE
        STRING "Choose the type of build." FORCE )
  # Set the possible values of build type for cmake-gui
  set_property ( CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS
                 "Debug" "Release" )
endif( NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES )
endmacro ( FORCE_DEFAULT_BUILD_TYPE )
