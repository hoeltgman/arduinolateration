# UltrasoundSensor {#ultrasound_sensor}

\startuml

class UltraSoundSensorPosition {
}

class UltraSoundSensor {
    Id
}

class UltraSoundSensorSystem {
}

UltraSoundSensor "1" *-- "1" UltraSoundSensorPosition
UltraSoundSensorSystem "1" o-- "*" UltraSoundSensor

\enduml
