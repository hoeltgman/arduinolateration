# Mathematics Library {#linear_algebra}

## Array Expressions

\startuml

class ArrayExpression {}
class Array {}
class ArrayDiff {}
class ArraySum {}
class ArrayScale {}
class ArrayUMinus {}
class ArrayRange {}
class ArrayForwardIterator

\enduml

## Matrices

\startuml

class Matrix {}

\enduml

## Matrix Transformations

\startuml

class GivensRotation {}
class HouseholderTransform

\enduml
