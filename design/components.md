# Design {#mvc}

\subpage model

\subpage view

\subpage controller

\subpage support

## Overview

The project is setup with an Model, View, Controller Architecture.

- The [Model](@ref model) components contain anything related to the data used for the underlying processing.
  Currently this includes:
  - Data structures to handle the measurements
- The [View](@ref view) components contain anything related to data visualisation and
  storage. Currently this includes:
  - Visualisation of the data on a OLED display
  - output to the serial port
  - storage of measurements on the SD file.
- The [Controller](@ref controller) components contain the code to influence the behavior of the
  system. Currently this includes:
  - The button control to turn on/off the recording.
- The [Support](@ref support) components contain
  - A math library

@startuml
[Model] as model [[model.html{Model}]]
[View] as view [[view.html{View}]]
[Controller] as controller [[controller.html{Controller}]]
[Support] as support [[support.html{Suppor}]]
model --> view
controller --> model
controller --> view
@enduml
