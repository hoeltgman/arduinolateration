# Echo Data {#echo_signal}

Our hardware setup consists of N ultrasonic sensors. Each Sensor can only listen
to its own signal, i.e. sensor K will never receive an echo which was sent out
by sensor L. A sensor and a measured echo signal form an EchoSample.

All sensors are triggered in cyclic order. Going once over all sensors and
gathering an EchoSample from each Sensors yields an EchoMeasurement.

An EchoMessage consists of up to M consecutive EchoMeasurements.

\startuml
!include echodata.iuml!0
\enduml
