// SPDX-License-Identifier: MIT

#include "Config.h"
#include "EchoMeasurement.h"
#include "EchoMessage.h"
#include "EchoSample.h"
#include "UltraSoundSensor.h"
#include "UltraSoundSensorPosition.h"
#include "UltraSoundSensorSystem.h"

#include <Arduino.h>
#include <SoftwareSerial.h>

/*
 * NOTE:
 * This is the main file for targetting the Arduino Unos.
 * This file is *not* part of the cmake build chain.
 * The cmake build chain can only construct the library and the unit tests.
 */

static SoftwareSerial echo_message_serial(Config::Model::RX_PORT_UNO, Config::Model::TX_PORT_UNO);

static Model::UltraSoundSensorSystem sensor_system;
static constexpr auto                serial_buffer_size = Model::EchoMessage::serial_buffer_size;
static unsigned char                 serialization_buffer[serial_buffer_size];
unsigned char                       *buffer_start = &serialization_buffer[0];
unsigned char                       *buffer_end   = nullptr;

void
setup()
{
        Serial.begin(Config::DEFAULT_BAUD_RATE);
        while (!Serial)
        {
                ;
        }
        Serial.println("System Setup Start");
        echo_message_serial.begin(Config::DEFAULT_BAUD_RATE);
        Serial.println("Setting PIN Modes");
        for (const auto &pin : Config::Model::TRIG_PIN_SENSOR_IDX)
        {
                pinMode(pin, OUTPUT);
                digitalWrite(pin, LOW);
        }
        for (const auto &pin : Config::Model::ECHO_PIN_SENSOR_IDX)
        {
                pinMode(pin, INPUT);
        }
        Serial.println("Setting Sensor System");
        for (size_t idx = 0; idx < Config::Model::NUM_SENSORS_PER_ROW; idx++)
        {
                Model::UltraSoundSensorPosition sensor_position(
                    Config::Model::SENSOR_POS[Config::POSITION_IDX_OFFSET + idx][0],
                    Config::Model::SENSOR_POS[Config::POSITION_IDX_OFFSET + idx][1],
                    Config::Model::SENSOR_POS[Config::POSITION_IDX_OFFSET + idx][2]);
                Model::UltraSoundSensor sensor(sensor_position,
                                               Config::Model::TRIG_PIN_SENSOR_IDX[idx],
                                               Config::Model::ECHO_PIN_SENSOR_IDX[idx],
                                               Config::Model::INTRA_SENSOR_DISTANCE);
                sensor_system.append(sensor);
        }
}

void
loop()
{
        for (auto &byte : serialization_buffer)
        {
                byte = 0;
        }

        Model::EchoMessage single_row_measurements;
        Serial.println("Beginning Message!");
        for (size_t counter = 0; counter < Config::Controller::MAX_NUM_MEASUREMENTS_PER_ROW; counter++)
        {
                Serial.println("Beginning Measurement!");
                Model::EchoMeasurement measurement;
                for (size_t idx = 0; idx < Config::Model::NUM_SENSORS_PER_ROW; idx++)
                {
                        const auto trig_pin        = Config::Model::TRIG_PIN_SENSOR_IDX[idx];
                        const auto echo_pin        = Config::Model::ECHO_PIN_SENSOR_IDX[idx];
                        const auto sensor          = sensor_system.get_sensor(idx);
                        const auto sensor_position = sensor.position();
                        // Record echo pulse duration
                        digitalWrite(trig_pin, HIGH);
                        delayMicroseconds(10);
                        digitalWrite(trig_pin, LOW);
                        delayMicroseconds(10);
                        auto echo_pulse_duration = pulseIn(echo_pin, HIGH);
                        Serial.print("Measured Pulse Duration: ");
                        Serial.println(echo_pulse_duration);
                        Model::EchoSample sample(sensor_position, sensor_position, echo_pulse_duration);
                        measurement.append(sample);
                }
                single_row_measurements.append(measurement);
        }
        buffer_end = single_row_measurements.serialize(buffer_start);
        Serial.println("Sending Message To Arduino Mega.");
        echo_message_serial.write(serialization_buffer, serial_buffer_size);
}
