// SPDX-License-Identifier: MIT

#include "Config.h"
#include "EchoMeasurement.h"
#include "EchoMessage.h"
#include "EchoSample.h"
#include "UltraSoundSensor.h"
#include "UltraSoundSensorPosition.h"
#include "UltraSoundSensorSystem.h"

#include <Arduino.h>

/*
 * NOTE:
 * This is the main file for targetting the Arduino Mega.
 * This file is *not* part of the cmake build chain.
 * The cmake build chain can only construct the library and the unit tests.
 */

static Model::UltraSoundSensorSystem sensor_system;
static constexpr auto                serial_buffer_size = Model::EchoMessage::serial_buffer_size;
static unsigned char                 serialization_buffer[serial_buffer_size];
unsigned char                       *buffer_start = &serialization_buffer[0];
unsigned char                       *buffer_end   = nullptr;
static Model::EchoMessage            messages[2];

void
setup()
{
        Serial.begin(Config::DEFAULT_BAUD_RATE);
        while (!Serial)
        {
                ;
        }
        Serial.println("System Setup Start");
        Serial1.begin(Config::DEFAULT_BAUD_RATE);
        while (!Serial1)
        {
                ;
        }
        Serial.println("Serial 1 Ready");
        Serial2.begin(Config::DEFAULT_BAUD_RATE);
        while (!Serial2)
        {
                ;
        }
        Serial.println("Serial 2 Ready");
}

void
loop()
{
        Serial.println("Entering Processing Loop.");
        Serial.print("Buffer Size: ");
        Serial.println(serial_buffer_size);

        for (auto &byte : serialization_buffer)
        {
                byte = 0;
        }

        Serial.println("Reading Message From Arduino 1.");
        if (Serial1.available())
        {
                auto counter = Serial1.readBytes(serialization_buffer, serial_buffer_size);
                Serial.print("Read Number Of Bytes From Serial 1: ");
                Serial.println(counter);
        }
        buffer_end = messages[0].deserialize(buffer_start);

        for (auto &byte : serialization_buffer)
        {
                byte = 0;
        }

        Serial.println("Reading Message From Arduino 2.");
        if (Serial2.available())
        {
                auto counter = Serial2.readBytes(serialization_buffer, serial_buffer_size);
                Serial.print("Read Number Of Bytes From Serial 2: ");
                Serial.println(counter);
        }

        buffer_end = messages[1].deserialize(buffer_start);
}
