// SPDX-License-Identifier: MIT

#include "Config.h"
#include "EchoMeasurement.h"
#include "EchoMessage.h"
#include "EchoSample.h"
#include "Parameters.h"
#include "SpeedOfSound.h"
#include "UltraSoundSensor.h"
#include "UltraSoundSensorPosition.h"
#include "UltraSoundSensorSystem.h"
#include "Vector.h"

#include <cstddef>
#include <cstdint>
#include <iostream>
#include <string>

/*
 * NOTE:
 * This is the main file for targetting the native Arduino build.
 * This file is also part of the cmake build chain.
 */

static Model::UltraSoundSensorSystem upper_sensor_system, lower_sensor_system;

static LinearAlgebra::Vector<float, 3> point_object_position;

static Model::SpeedOfSound &speed_sound = Model::SpeedOfSound::get_instance();

void
setup()
{
        std::cout << "Setup initiated." << std::endl;
        point_object_position[0] = 2.4;
        point_object_position[1] = 1.1;
        point_object_position[2] = 3.9;

        for (size_t idx = 0; idx < Config::Model::NUM_SENSORS_PER_ROW; idx++)
        {
                Model::UltraSoundSensorPosition sensor_position_upper(
                    Config::Model::SENSOR_POS[idx][0], Config::Model::SENSOR_POS[idx][1], Config::Model::SENSOR_POS[idx][2]);
                Model::UltraSoundSensor sensor_upper(sensor_position_upper,
                                                     Config::Model::TRIG_PIN_SENSOR_IDX[idx],
                                                     Config::Model::ECHO_PIN_SENSOR_IDX[idx],
                                                     Config::Model::INTRA_SENSOR_DISTANCE);
                upper_sensor_system.append(sensor_upper);

                Model::UltraSoundSensorPosition sensor_position_lower(
                    Config::Model::SENSOR_POS[Config::Model::NUM_SENSORS_PER_ROW + idx][0],
                    Config::Model::SENSOR_POS[Config::Model::NUM_SENSORS_PER_ROW + idx][1],
                    Config::Model::SENSOR_POS[Config::Model::NUM_SENSORS_PER_ROW + idx][2]);
                Model::UltraSoundSensor sensor_lower(sensor_position_lower,
                                                     Config::Model::TRIG_PIN_SENSOR_IDX[idx],
                                                     Config::Model::ECHO_PIN_SENSOR_IDX[idx],
                                                     Config::Model::INTRA_SENSOR_DISTANCE);
                lower_sensor_system.append(sensor_lower);
        }
}

void
loop()
{
        std::cout << "Loop initiated." << std::endl;
        Model::EchoMessage upper_row_measurements;
        for (size_t counter = 0; counter < Config::Controller::MAX_NUM_MEASUREMENTS_PER_ROW; counter++)
        {
                Model::EchoMeasurement measurement;
                for (size_t idx = 0; idx < Config::Model::NUM_SENSORS_PER_ROW; idx++)
                {
                        const auto                      sensor          = upper_sensor_system.get_sensor(idx);
                        const auto                      sensor_position = sensor.position();
                        LinearAlgebra::Vector<float, 3> sensor_position_vector;
                        sensor_position_vector[0] = sensor_position.x();
                        sensor_position_vector[1] = sensor_position.y();
                        sensor_position_vector[2] = sensor_position.z();
                        auto echo_pulse_duration  = 2.0f *
                                                   LinearAlgebra::norm<float>(point_object_position - sensor_position_vector) /
                                                   speed_sound.get_speed();
                        Model::EchoSample sample(sensor_position, sensor_position, echo_pulse_duration);
                        measurement.append(sample);
                }
                upper_row_measurements.append(measurement);
        }
        Model::EchoMessage lower_row_measurements;
        for (size_t counter = 0; counter < Config::Controller::MAX_NUM_MEASUREMENTS_PER_ROW; counter++)
        {
                Model::EchoMeasurement measurement;
                for (size_t idx = 0; idx < Config::Model::NUM_SENSORS_PER_ROW; idx++)
                {
                        const auto                      sensor          = lower_sensor_system.get_sensor(idx);
                        const auto                      sensor_position = sensor.position();
                        LinearAlgebra::Vector<float, 3> sensor_position_vector;
                        sensor_position_vector[0] = sensor_position.x();
                        sensor_position_vector[1] = sensor_position.y();
                        sensor_position_vector[2] = sensor_position.z();
                        auto echo_pulse_duration  = 2.0f *
                                                   LinearAlgebra::norm<float>(point_object_position - sensor_position_vector) /
                                                   speed_sound.get_speed();
                        Model::EchoSample sample(sensor_position, sensor_position, echo_pulse_duration);
                        measurement.append(sample);
                }
                lower_row_measurements.append(measurement);
        }
}

auto
main() -> int
{
        std::cout << "Native build of Arduino Lateration" << std::endl;
        const std::string version = std::to_string(Config::VERSION_MAJOR) + std::string(".") +
                                    std::to_string(Config::VERSION_MINOR) + std::string(".") +
                                    std::to_string(Config::VERSION_PATCH);
        std::cout << "Version: " << version << std::endl;
        std::cout << "This software is made available under a MIT license." << std::endl;
        setup();
        while (true)
        {
                loop();
        }
        return EXIT_SUCCESS;
}
