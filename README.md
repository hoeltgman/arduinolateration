# ArduinoLateration {#mainpage}
[![coverage report](https://gitlab.com/hoeltgman/arduinolateration/badges/master/coverage.svg)](https://gitlab.com/hoeltgman/arduinolateration/commits/master)
[![pipeline status](https://gitlab.com/hoeltgman/arduinolateration/badges/master/pipeline.svg)](https://gitlab.com/hoeltgman/arduinolateration/commits/master)
[![license](https://img.shields.io/badge/license-MIT-green)](https://opensource.org/licenses/MIT)

# Description

This repository contains the code and necessary descriptions for performing an
object localisation using an Arduino and several ultra sound sensors. The latest
version of the documentation is always available [here](https://hoeltgman.gitlab.io/arduinolateration/).
The source code repository is hosted on [gitlab](https://gitlab.com/hoeltgman/arduinolateration).

# Setup

The code is developed using PlatformIO.
The `tools` folder contains the pre-commit hooks for this repository. Currently
these are:
- platformio check (i.e. cppcheck)
- all unit tests must pass
- clang-format check (see https://github.com/barisione/clang-format-hooks)
- doxygen check: all functions must be documented
The hooks are all run with the script from here:
https://gist.github.com/mjackson/7e602a7aa357cfe37dadcc016710931b
The commit hooks ensure that the code format guidelines are followed, that there
are no severe issues in the code, and that every function has a least a doxygen
block.

In order to get the git hooks up and running:

- make sure you have clang-format and platformio up and running
- create a folder `pre-commit.d` inside `.git/hooks`
- create links from the folder `pre-commit.d` to `tools/git-pre-commit-format`, `tools/pio-check`, `tools/check-tests.sh` and `tools/check-doc.sh`.
  Alternatively copy these files from the `tools` folder to the `pre-commit.d` folder.
- create a file `.git/hooks/pre-commit` and copy the content of `tools/multiple-git-hooks.sh` into the file.

# Coding Conventions

- There is a `.clang-format` file to format the code automatically.
- 1 class per file. The file name corresponds with the class name. Naming is done in upper camel case.
  Exceptions to this rule are allowed for super classes that cannot exist without their subclasses (e.g. expression templates for containers).
- method and attribute names are written in lower snake case
- constants are written in all caps
- classes have a public, protected, private block in the specified order. A final public block with deleted methods may be added after the private block.
- all functions have exactly one return statement.

# Documentation

Documentation is generated using doxygen. Simply call

`doxygen Doxyfile`

in the root folder of the project.

# Building

The software can be build in several ways.

## Using PlatformIO

Use this if you want to upload the software to your Arduino. It is also possible
to launch the unit tests through PlatformIO.

## CMake

The software can also be build for testing purposes by using CMake. In this case
use the CMakeLists.txt file in the root folder of the project to initiate the
build process. The software should compile with any compiler which supports
C++14. It is also possible to use sanitizers when building with clang or gcc.

In addition the unit tests can be executed with ctest.

Note: The cmake build strategy requires a git submodule with the code of the
Unity testing framework. You may have to run

    git submodule init
    git submodule update

There is experimental support for sanitizers (assuming your compiler supports
these). To enable them, you must call

    cmake -DSANITIZE_ADDRESS:BOOL=ON -DSANITIZE_MEMORY:BOOL=ON -DSANITIZE_UNDEFINED:BOOL=ON

or by enabling the corresponding settings in the CMakeLists.txt in the project
root folder.

Compiler can be selected by calling

    CC=gcc && cmake . && cmake --build .

or whatever compiler you want to use.

# Unit tests and code coverage

Unit tests rely on the [unity framework](https://github.com/ThrowTheSwitch/Unity)
provided by [platformio](https://docs.platformio.org/en/latest/plus/unit-testing.html).
Code coverage information is generated for native builds. The current coverage
is displayed in the badge on top. Use lcov or gcovr to extract the information
and generate a more detailed overview yourself.

`gcovr -r . --html -o cov.html`

`lcov --capture --directory . --output-file coverage.info`
`genhtml coverage.info --output-directory out`

# Creating a compilation database

There several methods to create a compilation database (useful in conjunction
with clang-tidy):

1. Using [platformio](https://docs.platformio.org/en/latest/integration/compile_commands.html)
    platformio run -t compiledb
2. Using clang and cmake
    export CC=clang && export CXX=clang++ && cmake -G "Ninja" -DCMAKE_RC_COMPILER=llvm-rc
