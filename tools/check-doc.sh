#! /bin/bash

result=$(doxygen.exe Doxyfile 2> >(grep -c -i warning) > /dev/null);
exit $result
