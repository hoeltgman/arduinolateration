#!/usr/bin/env python

replacements = {}

version_file = open("version.txt", "r")
for line in version_file:
    tokens = line.split()
    replacements[tokens[0]] = tokens[1]

parameter_file = open("include/Parameters.h.in", "r")
parameter_file_data = parameter_file.read()

for key in replacements:
    source = "@" + key + "@"
    target = replacements[key]
    parameter_file_data = parameter_file_data.replace(source, target)

with open("include/Parameters.h", "w") as target_file:
    target_file.write(parameter_file_data)
