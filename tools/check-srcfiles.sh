#!/bin/bash

check_file() {
  file="${1}"
  if [ -f $file ]; then
    if [[ $file =~ .*\.(cpp|h|hpp) ]]; then
      echo $file
      result=$(clang-tidy --checks=-*,clang-analyzer-*,bugprone-*,cert-*,cppcoreguidelines-*,hicpp-*,misc-*,modernize-*,performance-*,readability-* -header-filter=.* ${1} 2> >(grep -c -i warning) > /dev/null);
    else
      return 0
    fi
  else
    return 0
  fi
  if [[ "$result" != 0 ]]; then
    exit $result
  fi
}

case "${1}" in
  --about )
    echo "Runs clang-tidy on source files"
    ;;
  * )
    for file in `git diff-index --cached --name-only HEAD` ; do
      echo "${file}"
      check_file "${file}"
    done
    ;;
esac
